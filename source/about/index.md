---
title: about
date: 2018-01-05 11:33:29
comments: false
---
# 关于我
 - 15级在校大学生
 - 计算机科学与技术专业
 - 96年出生在河北
---
# 关于本站
 - 首先感谢腾讯云提供学生服务器
 - 博客经过好长时间终于确定下来，原来的文章全都丢了
 - 域名使用阿里云注册使用
 - 感谢gitlab
---
# 联系方式
 - 腾讯QQ：1129603149
 - 邮箱：aeizzz.top@gmail.com
 - Telegram: https://t.me/aeizzz
 - 微博: https://weibo.com/p/1005055782437482/home
---
# 基础技能

## python
 - 爬虫
 - 学过scrapy框架，并写了一个简单的爬取新浪微博的小爬虫 [weibo](https://github.com/hong-ll/weibo)
 - 学过一点django，和flask这两个python web框架

## c&&c++
 - 参加acm-icpc,获得第八届山东省省赛铜奖，区域赛17年青岛赛区铜奖，第九届山东省省赛银奖
 - 16年蓝桥杯山东省赛一等奖，国赛三等奖
 - 17年蓝桥杯山东省赛一等奖

## java
 - 学过ssm(应该全忘了)

## 数据库
作为一个数据库原理挂科的我。没脸说数据库了
 - mysql
 - mongodb
 - redis
 - neo4j 正在学习

---
# 工具
 - 浏览器：主要为chrome，Firefox有时会用
 - 操作系统：windows(主用),ubuntu(服务器)
 - IDE：Pycharm,Idea,CodeBlocks,
 - 编辑器：vscode，Sublime Text3,Notepad++
 - 听音乐只用网易云