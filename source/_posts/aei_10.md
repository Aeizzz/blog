---
title: 使用docker部署gitlab应用
date: 2018-01-05 14:34:14
categories: linux
tags:
 - docker
 - gitlab
---
## 拉取镜像
首先拉去镜像，由于国内网速过慢，可以使用dao加速
`dao pull gitlab/gitlab-ce`

## 创建volumes
用户储存文件映射到宿主机，docker中是不存储数据的，并且映射出来后容易修改配置文件
存在三个volumes，分别为/mnt/volumes/gitlab下的config,logs,data目录
启动容器
```
sudo docker run --detach \
    --hostname git.xiaohuruwei.com \
    --publish 8443:443 --publish 8080:80 --publish 2222:22 \
    --name gitlab \
    --restart always \
    --volume /mnt/volumes/gitlab/config:/etc/gitlab \
    --volume /mnt/volumes/gitlab/logs:/var/log/gitlab \
    --volume /mnt/volumes/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest
```
由于以后有可能会修改内容，所以我写了一个docker-compose.yml可以简单管理,如下
```
web:
  image: 'gitlab/gitlab-ce:latest'
  restart: always
  hostname: 'gitlab.com'
  environment:
    GITLAB_OMNIBUS_CONFIG: |
      external_url 'http://gitlab.com'
  ports:
    - '8080:80'
    - '8443:443'
    - '2222:22'
  volumes:
    - '/root/gitlab/config:/etc/gitlab'
    - '/root/gitlab/logs:/var/log/gitlab'
    - '/root/gitlab/data:/var/opt/gitlab'
```
其中内容根据自己需要进行修改

使用`docker-compose up -d`启动

## 配置nginx, 支持https

nginx 配置文件
```
server {
    listen 80;
    server_name gitlab.com;
    access_log /var/log/nginx/gitlab.xiaohuruwei.access.log;
    error_log /var/log/nginx/gitlab.xiaohuruwei.error.log;
    rewrite ^ https://gitlab.com;
}

```
https proxy
```
server {
    listen       443 ssl;
    server_name  gitlab.com;
    access_log /var/log/nginx/https-gitlab.access.log;
    error_log /var/log/nginx/https-gitlab.error.log;
    # ssl 证书配置，这里使用的是自己生成的证书，在访问时会提示证书问题，选择相信即可。
    # 如果想要公认的证书，需要在网络上的一些授权中心生成
    ssl on;
    ssl_certificate /etc/nginx/ssl/getbase.crt;
    ssl_certificate_key /etc/nginx/ssl/getbase_nopass.key;
    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass https://localhost:8443;
    }
}

```

## 开放gitlab的https的支持
仅仅由nginx反向代理https是不行的，因为还需要打开gitlab的https支持
 - 修改配置文件，在/mnt/volumes/gitlab/config/ 目录下的gitlab.rb中添加：
 ```
 # note the 'https' below
 external_url "https://gitlab.example.com"
 ```
 - 新建ssl目录，同时在该目录下添加ssl证书文件，命名要与上述域名中保持一致如
 ```
 git.xiaohuruwei.com.crt
 git.xiaohuruwei.com.key
 ```
 - 重新启动容器
  `docker-compose restart`

## 访问gitlab测试
 - 打开web界面，默认登录名为root，密码为5iveL!fe（已经改为厘米脚印的默认密码）

## ssh方式访问
因为是使用docker部署的，通过ssh方式(比如git clone git@gitlab.com)访问会有两层认证:

一层是freelancer服务器的认证
另一层是gitlab的认证。
后者需要使用ssh-key
前者可能需要ssh本身的反向代理(现在使用的nginx不支持除http，https以外的反向代理)，

现在发现使用端口转发的形式比较困难，但是可以改变默认的gitlab的ssh端口为非标准端口：
直接修改gitlab配置文件中的变量：
```
gitlab_shell_ssh_port = 2222
```
然后重新启动docker容器，就可以在web界面中看到相应的ssh地址发生了改变:
ssh://git@gitlab.com:2222/root/test.git 然后就直接可以继续使用git clone来继续操作了

---

结束
基本的使用就可以了，还可以自己配置邮件系统，关闭注册，使用gitlab-ci，等