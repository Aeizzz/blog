---
title: 乘法逆元
date: 2018-01-05 13:16:47
categories: Algorithm
tags: 数学
---
## 学习乘法逆元

学习地址[Aireen Ye][1]
原来不知道乘法逆元是什么，也不怎么理解，最近看的博客才明白点，顺便记录下来
乘法逆元：$ ab≡1(\mod p) $则称为$b$为在$\mod p$下的$a$的乘法逆元

## 乘法逆元的求解方法
本文介绍两种求解乘法逆元的方法
还有一种线性求逆元我不会，就没写
### 扩展欧几里德求逆元
因为 $ab≡1(\mod p)$，所以设q满足 $a\*b+q\*p=1$
所以可以用扩展欧几里德求的关于$b,q$的方程$a\*b+q\*p=1$的一组解
```
typedef long long ll;
ll exgcd(ll a,ll b,ll & x,ll & y){
    if(b == 0){
        x = 1;
        y = 0;
        return a;
    }
    ll r = exgcd(b, a%b, x, y);
    ll t = y;
    y = x - (a/b)*y;
    x = t;
    return r;
}
```
判断exgcd的返回值是不是1
### 费马小定理求逆元
因为[费马小定理][2]中有假如a是一个整数，p是一个质数，那么 $ a^{p}-a$是p的倍数，可以表示为\\[ a^{p}\equiv a{\pmod  {p}}\\]
如果a不是p的倍数，这个定理也可以写成\\[a^{{p-1}}\equiv 1{\pmod {p}}\\]
所以当p是质数的时候就有$ a^{p-1} \equiv 1 { \pmod  {p}} $，然后就可以得到$a*a^{p-2}\equiv 1{\pmod  {p}}$可以得到$a^{p-2}$是$a$的乘法逆元然后用快速幂求得$a^{p-2}$
```
typedef long long ll;
inline ll power(ll x,ll k){
    ll ret=1;
    while(k){
        if(k&1) ret=ret*x%p;
        x=x*x%p;k>>=1;
    }
    return ret;
}
inline ll inver{
    return power(a,p-2);
}
```



  [1]: http://aireenye.leanote.com/post/Multiplicative-Inverse
  [2]: https://zh.wikipedia.org/wiki/%E8%B4%B9%E9%A9%AC%E5%B0%8F%E5%AE%9A%E7%90%86