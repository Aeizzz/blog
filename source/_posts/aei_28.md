---
title: Lucas
date: 2018-01-05 13:09:14
categories: Algorithm
tags: 数学
---
# Lucas(卢卡斯)定理

---
首先卢卡斯定理是什么？有什么用？
## 维基百科
在数论中，Lucas定理用于计算二项式系数c(n,m)被质数 p 除的所得的余数。
## 百度百科
Lucas定理是用来求 c(n,m) mod p，p为素数的值。


---
现在知道卢卡斯定理有什么用了吧，<br>
在算法竞赛中，有很多的数论题目需要求大数的二项式定理；

二项式定理
c(n,m)=c(n-1,m)+c(n-1,m-1);递推或递归公式求解<br>
(提示：c(n,m)%p=n!/(m!(n-m)!)%p;）

这样求，在小数方面可以求解，但是当n,m,p很大的时候，递推用的时间很大，所以这样要用lucas定理来求解；

---
在这里证明方式我就不多写了，，，如果想参考证明方法的话可以参考[百度百科(lucas)][1]·[维基百科(lucas)][2]

----
接下来就是代码实现了：
```
#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn = 1e5;
ll n,m,p;
ll fac[maxn];
void init(){
    int i;
    fac[0]=1;
    for(int i=1;i<=p;i++){
        fac[i]=fac[i-1]*i%p;
    }
}
//快速幂
ll q_pow(ll a,ll b){
    ll ans = 1;
    while(b){
        if(b&1) ans = ans * a%p;
        b>>=1;
        a=a*a%p;
    }
    return ans;
}

//这里面有乘法逆元，运用费马小定理，这就是为什么p必须是素数的原因
ll c(ll n,ll m){
    if(m>n) return 0;
    return fac[n]*q_pow(fac[m]*fac[n-m],p-2)%p;
}
ll lucas(ll n,ll m){
    if(m==0) return 1;
    else return (c(n%p,m%p)*lucas(n/p,m/p))%p;
}
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        scanf("%lld%lld%lld",&n,&m,&p);
        init();
        printf("%lld\n",lucas(n,m));
    }
    return 0;
}

```

---
在这里说一下利用费马小定理求乘法逆元

在求解除法取模问题(a/b)%m时，我们可以转化为(a%(b∗m))/b， 
但是如果b很大，则会出现爆精度问题，所以我们避免使用除法直接计算。 
可以使用逆元将除法转换为乘法,

在p是素数的情况下，对任意整数x都有x^p≡x%p。 
如果x无法被p整除，则有x^(p−1)≡1(modp)。 
可以在p为素数的情况下求出一个数的逆元，x∗x^(p−2)≡1(modp)，x^(p−2)即为逆元。
这也就是上面快速幂的作用




  [1]: http://baike.baidu.com/subview/1089303/7279451.htm
  [2]: https://zh.wikipedia.org/wiki/%E5%8D%A2%E5%8D%A1%E6%96%AF%E5%AE%9A%E7%90%86
