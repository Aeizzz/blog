---
title: 使用 kubeadm 安装k8s集群
date: 2018-05-11 11:39:00
categories: K8S
tags: K8S
top: false
---

## 一 环境规划
|角色|IP|组件|
|:--:|:--:|:--:|
| k8smaster|192.168.172.128 | 全部|
| k8snode1|192.168.172.129 | 全部|
| k8snode2|192.168.172.130 | 全部|

全部组件
```
docker1.13.1-53 
kubelet-1.10.0-0.x86_64
kubeadm-1.10.0-0.x86_64 
kubectl-1.10.0-0.x86_64
kubernetes-cni-0.6.0-0.x86_64

```

## 二 Master 和 Node 通用安装
### 安装docker
```
curl https://releases.rancher.com/install-docker/17.03.sh | sh
```
### 关闭系统防火墙
```
systemctl stop firewalld
systemctl disable firewalld
```
关闭SElinux:
```
setenforce 0
```
关闭swap：
```
swapoff -a
永久禁用swap可以直接修改/etc/fstab文件，注释掉swap项。swapoff -a 只是临时禁用，下次关机重启又恢复原样；
```
设置etc/hosts：
```
vim /etc/hosts


127.0.0.1 localhost
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.172.128 k8smaster
192.168.172.129 k8snode1
192.168.172.130 k8snode2
```
配置系统内核参数使流过网桥的流量也进入iptables/netfilter框架中，在/etc/sysctl.conf中添加以下配置：
```
echo "net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.conf
sysctl -p
```
配置阿里K8S YUM源：
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64 
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg 
http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg 
EOF
```
安装kubeadm和相关工具包
```
yum -y install kubelet kubeadm kubectl kubernetes-cni
```
设置加速地址和私有仓库
```
echo '{ "insecure-registries":["210.44.71.43:8080"] }' > /etc/docker/daemon.json
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://88bbf89e.m.daocloud.io
```


启动 docker 服务和 kubelet服务，并设置开机启动：
```
systemctl enable docker && systemctl start docker
systemctl enable kubelet && systemctl start kubelet
```
下载K8S相关镜像:
```bash
docker pull keveon/pause-amd64:3.1
docker tag keveon/pause-amd64:3.1 k8s.gcr.io/pause-amd64:3.1 
docker pull keveon/etcd-amd64:3.1.12
docker tag keveon/etcd-amd64:3.1.12 k8s.gcr.io/etcd-amd64:3.1.12 
docker pull keveon/kube-apiserver-amd64:v1.10.0
docker tag keveon/kube-apiserver-amd64:v1.10.0 k8s.gcr.io/kube-apiserver-amd64:v1.10.0 
docker pull keveon/kube-scheduler-amd64:v1.10.0
docker tag keveon/kube-scheduler-amd64:v1.10.0 k8s.gcr.io/kube-scheduler-amd64:v1.10.0 
docker pull keveon/kube-controller-manager-amd64:v1.10.0
docker tag keveon/kube-controller-manager-amd64:v1.10.0 k8s.gcr.io/kube-controller-manager-amd64:v1.10.0 
docker pull keveon/kube-proxy-amd64:v1.10.0
docker tag keveon/kube-proxy-amd64:v1.10.0 k8s.gcr.io/kube-proxy-amd64:v1.10.0 
docker pull keveon/k8s-dns-dnsmasq-nanny-amd64:1.14.8
docker tag keveon/k8s-dns-dnsmasq-nanny-amd64:1.14.8 k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64:1.14.8 
docker pull keveon/k8s-dns-kube-dns-amd64:1.14.8
docker tag keveon/k8s-dns-kube-dns-amd64:1.14.8 k8s.gcr.io/k8s-dns-kube-dns-amd64:1.14.8 
docker pull keveon/kubernetes-dashboard-amd64:v1.8.3
docker tag keveon/kubernetes-dashboard-amd64:v1.8.3 k8s.gcr.io/kubernetes-dashboard-amd64:v1.8.3 
docker pull keveon/k8s-dns-sidecar-amd64:1.14.8
docker tag keveon/k8s-dns-sidecar-amd64:1.14.8 k8s.gcr.io/k8s-dns-sidecar-amd64:1.14.8
```

## 三、初始化安装K8S Master
执行kubeadm init:
```
kubeadm init --kubernetes-version=v1.10.0 --pod-network-cidr=10.244.0.0/16
```
保存最后一句，用来以后加入集群所用
```
 kubeadm join 192.168.172.128:6443 --token 5ppdi6.huri9v10cu46i3du --discovery-token-ca-cert-hash sha256:626ec9040ca5b6cef94b79aec0a4a79dab958c6a44ef19594f85bc689a075eb4
```
如果出现错误
```
[kubelet-check] The HTTP call equal to 'curl -sSL http://localhost:10255/healthz' failed with error: Get http://localhost:10255/healthz: dial tcp [::1]:10255: getsockopt: connection refused.
[kubelet-check] It seems like the kubelet isn't running or healthy.
[kubelet-check] The HTTP call equal to 'curl -sSL http://localhost:10255/healthz' failed with error: Get http://localhost:10255/healthz: dial tcp [::1]:10255: getsockopt: connection refused.
```
则配置
https://stackoverflow.com/questions/47216577/cant-install-kubernetes-cluster-on-centos-7
update kubelet to use cgroupfs
```
sed -i -E 's/--cgroup-driver=systemd/--cgroup-driver=cgroupfs/' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
systemctl daemon-reload
systemctl restart kubelet.service

kubeadm reset 

#再重新init
```


配置kubectl认证信息：
```
#对于非root用户
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
#对于root用户
export KUBECONFIG=/etc/kubernetes/admin.conf
```

安装flannel网络：
```
mkdir -p /etc/cni/net.d/
cat <<EOF> /etc/cni/net.d/10-flannel.conf
{
"name": "cbr0",
"type": "flannel”,
“delegate”: {
“isDefaultGateway”: true
}
}
EOF

mkdir /usr/share/oci-umount/oci-umount.d -p
mkdir /run/flannel/
cat <<EOF> /run/flannel/subnet.env
FLANNEL_NETWORK=10.244.0.0/16
FLANNEL_SUBNET=10.244.1.0/24
FLANNEL_MTU=1450
FLANNEL_IPMASQ=true
EOF

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

## 四、初始化安装node
让k8snode1加入集群：
```
kubeadm join 192.168.172.128:6443 --token 5ppdi6.huri9v10cu46i3du --discovery-token-ca-cert-hash sha256:626ec9040ca5b6cef94b79aec0a4a79dab958c6a44ef19594f85bc689a075eb4
```

## 五、验证K8S Master是否搭建成功
查看节点状态：
```
kubectl get nodes
```
查看pods状态：
```
kubectl get pods --all-namespaces
```
查看K8S集群状态:
```
kubectl get cs
```

## 六、安装 Dashboard
根据大佬改过官方的
clone yaml文件
```
git clone https://github.com/gh-Devin/kubernetes-dashboard.git
```
更改文件heapster.yaml 中 heapster 镜像 为`rancher/heapster-amd64:v1.4.0`  
更改kubernetes-dashboard.yaml 文件中    
`147行  serviceAccountName: kubernetes-dashboard`更改为`serviceAccountName: kubernetes-dashboard-admin`

```
kubectl  -n kube-system create -f .

直接运行
```

最后完成启动以后，访问IP:30090
