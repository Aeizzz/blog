---
title: 解决linux下的vim中文乱码
date: 2018-01-05 13:25:50
categories: linux
tags: vim
---
打开`/etc/vim/vimrc`在后面增加
```
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8
```