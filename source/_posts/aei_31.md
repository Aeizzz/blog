---
title: scrapy中间件的使用
date: 2018-01-05 13:26:31
categories: 爬虫
tags: scrapy
---
scarpy的中间件能解决很多问题，比如user-agent的更换，cookies的更换，以及代理IP的更换，等更多.
中间件主要是用做在reques请求之前进行操作，或者response时进行操作，以及等多(还没有学会)
下面讲几种的使用  
这里说的中间件是下载器中间件  
<!-- more -->
## User-Agent的更换
首先激活中间件  
在settings中的DOWNLOADER_MIDDLEWARES下配置需要激活的中间件，后面的数值是优先度  
后面的数字表示优先级。数字越小，优先级越高，越先执行。例如如果你有一个更换代理的中间件，还有一个更换Cookie的中间件，那么更换代理的中间件显然是需要在更换Cookie的中间件之前运行的。如果你把这个数字设为None，那么就表示禁用这个中间件。
```
DOWNLOADER_MIDDLEWARES = {
    'weibo.middlewares.UserAgentMiddleware': 401,
}
```
在middlewares中编写中间件
```
class UserAgentMiddleware(UserAgentMiddleware):
    def process_request(self,request,spider):
        agent = random.choice(agents)
        request.headers.setdefault("User-Agent",agent)
```
导入写好的User-Agent的文件如
```
agents = [
    "Mozilla/5.0 (Linux; U; Android 2.3.6; en-us; Nexus S Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
    "Avant Browser/1.2.789rel1 (http://www.avantbrowser.com)",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5",
    "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.310.0 Safari/532.9",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
    "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.601.0 Safari/534.14",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.27 (KHTML, like Gecko) Chrome/12.0.712.0 Safari/534.27",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.24 Safari/535.1",
    "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2"
]
```
这样一个更换User-Agent的中间件就完成了


## 其他中间件
如代理
```
class ProxyMiddleware(object):
  # overwrite process request
  def process_request(self, request, spider):
    # Set the location of the proxy
    request.meta['proxy'] = "http://PROXY_IP:PORT"

    # Use the following lines if your proxy requires authentication
    proxy_user_pass = "USERNAME:PASSWORD"
    # setup basic authentication for the proxy
    encoded_user_pass = base64.encodestring(proxy_user_pass)
    request.headers['Proxy-Authorization'] = 'Basic ' + encoded_user_pass

```
---

如果在response时使用中间件呢，比如请求时cookies失效，一般网站就会重定向到登陆界面
那么就使用下面这个函数
```
process_response(request, response, spider)
```
文档中的说法是
```
process_request() 必须返回以下之一: 返回一个 Response 对象、 返回一个 Request 对象或raise一个 IgnoreRequest 异常。

如果其返回一个 Response (可以与传入的response相同，也可以是全新的对象)， 该response会被在链中的其他中间件的 process_response() 方法处理。

如果其返回一个 Request 对象，则中间件链停止， 返回的request会被重新调度下载。处理类似于 process_request() 返回request所做的那样。

如果其抛出一个 IgnoreRequest 异常，则调用request的errback(Request.errback)。 如果没有代码处理抛出的异常，则该异常被忽略且不记录(不同于其他异常那样)。

参数:	
    request (Request 对象) – response所对应的request
    response (Response 对象) – 被处理的response
    spider (Spider 对象) – response所对应的spider

```


