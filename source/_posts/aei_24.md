---
title: hexo-blog的持续集成
date: 2018-01-05 19:46:16
categories: 持续集成
tags: hexo
top: true
---

更新blog已经托管到gitlab上，gitlab ci/cd 负责支持构建和发布，感谢gitlab

-----

原来由于hexo博客因为更换电脑不方便，导致很多博客丢失，并且更换使用别的博客，现在又换了回来
主要原因是，hexo主题还算挺好看，另外是别的博客太丑了
并且现在我解决的博客因更换电脑丢失的问题，
使用gitlab-ci+gitlan-runner持续集成，发布到自己的服务器上

## 关于构建服务器
首先hexo要在这个上面构建，所以要提前安装环境
 - node环境
Alternatively for Node.js 9:
```
curl --silent --location https://rpm.nodesource.com/setup_9.x | sudo bash -
```
Then install:
```
sudo yum -y install nodejs
```
 - 安装hexo
```
npm install hexo-cli -g
```


## 安装gitlab-runner
我使用的centos
 - 添加源
```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | sudo bash
```
 - 安装
```
yum install gitlab-ci-multi-runner
```

## gitlan-ci设置
在gitlab上开启gitlab-ci,如图
![](http://olz5306lf.bkt.clouddn.com/TIM%E5%9B%BE%E7%89%8720180105195252.png)

## 注册gitlab-runner
首先在gitlan上查看url和token
如下
![](http://olz5306lf.bkt.clouddn.com/TIM%E6%88%AA%E5%9B%BE20180105195724.png)

然后在自己服务器上运行gitlab-runner注册一个runner
```
gitlab-ci-multi-runner register
#引导会让你输入gitlab的url，输入自己的url，例如http://gitlab.example.com/
#引导会让你输入token，去相应的项目下找到token，例如ase12c235qazd32
#引导会让你输入tag，一个项目可能有多个runner，是根据tag来区别runner的，输入若干个就好了，比如web,hook,deploy
#引导会让你输入executor，这个是要用什么方式来执行脚本，图方便输入shell就好了。
```

## 编写.gitlab-ci.yml
在项目根目录下编写.gitlab-ci.yml这样在push之后，gitlab-ci就会自动识别来解析了。
```
stages:
  - deploy
deploy:
    stage: deploy
    script:
      - deploy Example_Group Example_Project
    only:
      - master
    tags:
      - shell

```
这里我们只有一个stage是deploy。only指定了只有在master分支push的时候才会被执行。tags是shell，对应了刚才注册runner的时候的tags
最重要的script部分deploy Example_Group Example_Project，这里是一条shell指令，为了方便通用性，deploy是我在服务器上编写的一个脚本，传入参数是Example_Group Example_Project分别是项目组名和项目名。执行这一条指令就能够自动部署到/xxx/Example_Group/Example_Project的服务器目录下。那么随便什么项目都用这个格式去套就好了，这样新项目的自动部署也不需要登录到服务器上去修改了。

以下是我编写的deploy脚本个.gitlab-ci.yml文件
deploy
```
#!/bin/bash
if [ $# -ne 2 ]
then
        echo "arguments error!"
        exit 1
else
        deploy_path="/home/deploy/$1/$2"
        if [ ! -d "$deploy_path" ]
        then
                project_path="https://www.7326it.club/"$1/$2".git"
                git clone $project_path $deploy_path
        else
                cd $deploy_path
                git pull
        fi
fi
```

.gitlab-ci.yml
```
stages:
  - deploy
deploy:
    stage: deploy
    script:
      - pwd
      - sh /home/gitlab-runner/deploy hong-ll blog
      - cd /home/deploy/hong-ll/blog
      - pwd
      - cnpm install
      - hexo clean
      - hexo g
      - rm -r /home/tmp
      - mv /home/www /home/tmp
      - mkdir /home/www
      - cp -r public/* /home/www/
    only:
      - master
    tags:
      - '7326'

```
shell脚本不怎么会写，差不多先这样吧
## push项目

把自己本地的项目写好push到gitlab服务器他就会自己构建，发布生成前端文件到nginx下


