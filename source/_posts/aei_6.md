---
title: 第八届蓝桥杯省赛B组java
date: 2018-03-22 20:03:15
categories: 蓝桥
tags: 蓝桥
top: false
---
# 第八届蓝桥杯省赛B组java

题解代码  https://github.com/Aeizzz/code/tree/master/src/lanqiao/eight
<!-- more -->

关于题目：
## 第一题
```
标题： 购物单

小明刚刚找到工作，老板人很好，只是老板夫人很爱购物。老板忙的时候经常让小明帮忙到商场代为购物。小明很厌烦，但又不好推辞。

这不，XX大促销又来了！老板夫人开出了长长的购物单，都是有打折优惠的。
小明也有个怪癖，不到万不得已，从不刷卡，直接现金搞定。
现在小明很心烦，请你帮他计算一下，需要从取款机上取多少现金，才能搞定这次购物。

取款机只能提供100元面额的纸币。小明想尽可能少取些现金，够用就行了。
你的任务是计算出，小明最少需要取多少现金。

以下是让人头疼的购物单，为了保护隐私，物品名称被隐藏了。
 -----------------
 ****     180.90       88折
 ****      10.25       65折
 ****      56.14        9折
 ****     104.65        9折
 ****     100.30       88折
 ****     297.15        半价
 ****      26.75       65折
 ****     130.62        半价
 ****     240.28       58折
 ****     270.62        8折
 ****     115.87       88折
 ****     247.34       95折
 ****      73.21        9折
 ****     101.00        半价
 ****      79.54        半价
 ****     278.44        7折
 ****     199.26        半价
 ****      12.97        9折
 ****     166.30       78折
 ****     125.50       58折
 ****      84.98        9折
 ****     113.35       68折
 ****     166.57        半价
 ****      42.56        9折
 ****      81.90       95折
 ****     131.78        8折
 ****     255.89       78折
 ****     109.17        9折
 ****     146.69       68折
 ****     139.33       65折
 ****     141.16       78折
 ****     154.74        8折
 ****      59.42        8折
 ****      85.44       68折
 ****     293.70       88折
 ****     261.79       65折
 ****      11.30       88折
 ****     268.27       58折
 ****     128.29       88折
 ****     251.03        8折
 ****     208.39       75折
 ****     128.88       75折
 ****      62.06        9折
 ****     225.87       75折
 ****      12.89       75折
 ****      34.28       75折
 ****      62.16       58折
 ****     129.12        半价
 ****     218.37        半价
 ****     289.69        8折
 --------------------

需要说明的是，88折指的是按标价的88%计算，而8折是按80%计算，余者类推。
特别地，半价是按50%计算。

请提交小明要从取款机上提取的金额，单位是元。
答案是一个整数，类似4300的样子，结尾必然是00，不要填写任何多余的内容。

```
使用excel来计算，最总虽有商品的综合为5136.86元  所以需要5200元
详细见上面链接中的 蓝桥A.xlsx 文件

## 第二题
```
标题：纸牌三角形

A,2,3,4,5,6,7,8,9 共9张纸牌排成一个正三角形（A按1计算）。要求每个边的和相等。
下图就是一种排法（如有对齐问题，参看p1.png）。

      A
     9 6
    4   8
   3 7 5 2

这样的排法可能会有很多。

如果考虑旋转、镜像后相同的算同一种，一共有多少种不同的排法呢？

请你计算并提交该数字。

注意：需要提交的是一个整数，不要提交任何多余内容。
```

/*
使用c++很容易完成，因为是填空题，就不一定要使用java语言
直接9个数全排列，然后计算
对于镜像: 一个三角形有一个镜像
对于旋转; 一个三角形有两个旋转
所以一个三角形加上镜像和旋转一共有6种，最后得出结果除6即可

*/

```
#include<cstdio>
#include<algorithm>
using namespace std;
int a[9] = {1,2,3,4,5,6,7,8,9};

int main(){
    int sum=0;
    do{
        int x1 = a[0] + a[1] + a[2] + a[3];
        int x2 = a[3] + a[4] + a[5] + a[6];
        int x3 = a[6] + a[7] + a[8] + a[0];
        if(x1 == x2 && x1 == x3){
            for(int i=0;i<9;i++){
                printf("%d ",a[i]);
            }
            sum++;
            printf("\n");
        }
    }while(next_permutation(a,a+9));
    printf("%d",sum/6);


    return 0;
}
```

## 第三题
```
标题：承压计算

X星球的高科技实验室中整齐地堆放着某批珍贵金属原料。

每块金属原料的外形、尺寸完全一致，但重量不同。
金属材料被严格地堆放成金字塔形。

                             7
                            5 8
                           7 8 8
                          9 2 7 2
                         8 1 4 9 1
                        8 1 8 8 4 1
                       7 9 6 1 4 5 4
                      5 6 5 5 6 9 5 6
                     5 5 4 7 9 3 5 5 1
                    7 5 7 9 7 4 7 3 3 1
                   4 6 4 5 5 8 8 3 2 4 3
                  1 1 3 3 1 6 6 5 5 4 4 2
                 9 9 9 2 1 9 1 9 2 9 5 7 9
                4 3 3 7 7 9 3 6 1 3 8 8 3 7
               3 6 8 1 5 3 9 5 8 3 8 1 8 3 3
              8 3 2 3 3 5 5 8 5 4 2 8 6 7 6 9
             8 1 8 1 8 4 6 2 2 1 7 9 4 2 3 3 4
            2 8 4 2 2 9 9 2 8 3 4 9 6 3 9 4 6 9
           7 9 7 4 9 7 6 6 2 8 9 4 1 8 1 7 2 1 6
          9 2 8 6 4 2 7 9 5 4 1 2 5 1 7 3 9 8 3 3
         5 2 1 6 7 9 3 2 8 9 5 5 6 6 6 2 1 8 7 9 9
        6 7 1 8 8 7 5 3 6 5 4 7 3 4 6 7 8 1 3 2 7 4
       2 2 6 3 5 3 4 9 2 4 5 7 6 6 3 2 7 2 4 8 5 5 4
      7 4 4 5 8 3 3 8 1 8 6 3 2 1 6 2 6 4 6 3 8 2 9 6
     1 2 4 1 3 3 5 3 4 9 6 3 8 6 5 9 1 5 3 2 6 8 8 5 3
    2 2 7 9 3 3 2 8 6 9 8 4 4 9 5 8 2 6 3 4 8 4 9 3 8 8
   7 7 7 9 7 5 2 7 9 2 5 1 9 2 6 5 3 9 3 5 7 3 5 4 2 8 9
  7 7 6 6 8 7 5 5 8 2 4 7 7 4 7 2 6 9 2 1 8 2 9 8 5 7 3 6
 5 9 4 5 5 7 5 5 6 3 5 3 9 5 8 9 5 4 1 2 6 1 4 3 5 3 2 4 1
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X

其中的数字代表金属块的重量（计量单位较大）。
最下一层的X代表30台极高精度的电子秤。

假设每块原料的重量都十分精确地平均落在下方的两个金属块上，
最后，所有的金属块的重量都严格精确地平分落在最底层的电子秤上。
电子秤的计量单位很小，所以显示的数字很大。

工作人员发现，其中读数最小的电子秤的示数为：2086458231

请你推算出：读数最大的电子秤的示数为多少？

```

同样使用c++，最小和他成比例求出最大

```
#include <cstdio>
#include <algorithm>

using namespace std;
double a[30][30];

int main() {
//    freopen("C.txt", "r", stdin);

    for (int i = 0; i < 29; i++) {
        for (int j = 0; j <= i; j++) {
            scanf("%lf", &a[i][j]);
        }
    }
//    for (int i = 0; i < 29; i++) {
//        for (int j = 0; j <= i; j++) {
//            printf("%lf ", a[i][j]);
//        }
//        printf("\n");
//    }

    for (int i = 1; i < 30; i++) {
        for (int j = 0; j <= i; j++) {
            if (j == 0) a[i][j] += a[i - 1][j] / 2;
            else if (j == i) {
                a[i][j] += a[i - 1][j - 1] / 2;
            } else {
                a[i][j] += (a[i - 1][j - 1] + a[i - 1][j]) / 2;
            }
        }
    }
    double minx = 9999999;
    double maxx = -1;
    for (int i = 0; i < 30; i++) {
        printf("%lf ", a[29][i]);
        minx = min(minx, a[29][i]);
        maxx = max(maxx, a[29][i]);

    }
    printf("\n");
    printf("%lf %lf     ",minx,maxx);
    printf("%lf",2086458231/minx*maxx);


}

```

## 第四题
```
标题：魔方状态

 二阶魔方就是只有2层的魔方，只由8个小块组成。
 如图p1.png所示。

 小明很淘气，他只喜欢3种颜色，所有把家里的二阶魔方重新涂了颜色，如下：

 前面：橙色
 右面：绿色
 上面：黄色

 下面：橙色
 后面：黄色

 请你计算一下，这样的魔方被打乱后，一共有多少种不同的状态。

 如果两个状态经过魔方的整体旋转后，各个面的颜色都一致，则认为是同一状态。

 请提交表示状态数的整数，不要填写任何多余内容或说明文字。

```

题目大意
有一个魔方，上  黄色 | 下  橙色  | 左  绿色 |  右  绿色 | 前 橙色 | 后 黄色

 问魔方总共有多少种状态。

 我的答案：216

 我的思路：刚拿到这题有点懵逼，简单分析了一下，其实魔方总共八块，这八块其实就三种颜色

 4块是绿黄橙，2块是绿橙橙，2块是绿黄黄。

 我们就可以把该题简化为四个1，两个2，两个3的排序问题。

 需要注意的是11112233和33221111是一样的。
 
 ```JAVA
 public class D {
    static int[] v = new int[8];
    static int[] s = new int[8];
    static ArrayList<String> a = new ArrayList<String>();

    public static void main(String[] args) {
        s(0);
        System.out.println(a.size());
    }
    public static void s(int code){
        if(code==8){
            String t = "";
            String t1 = "";//t正序，t1倒序
            for (int i = 0; i < s.length; i++) {
                t += s[i]+"";
                t1 += s[s.length-1-i]+"";
            }
            if(!a.contains(t)&&!a.contains(t1)){
                a.add(t);
            }
            return;
        }
        for(int i=0;i<8;i++){
            if(v[i]==0){
                v[i]=1;
                if(i<4){
                    s[code] = 1;
                }
                else if(i<6){
                    s[code] = 2;
                }
                else if(i<8){
                    s[code] = 3;
                }
                s(code+1);
                v[i]=0;
            }
        }
    }
}

 ```
 
 ```CPP
 #include<cstdio>
#include<algorithm>
#include <string>
#include <set>

using namespace std;
string s = "11112233";
set<string> se;

string re(string s) {
    string t = "";
    for (int i = 0; i < s.size(); i++) {
        t += s[s.size() - 1 - i];
    }
    return t;
}


int main() {
    int sum = 0;
    do {
        if (!se.count(s) && !se.count(re(s))) {
            se.insert(s);
        }

        //se.insert(re(s));
    } while (next_permutation(s.begin(), s.end()));
    printf("%d", se.size());


    return 0;
}

 ```
 
 ## 第五题
 ```
标题：取数位
求1个整数的第k位数字有很多种方法。
以下的方法就是一种。
<p>
public class Main
{
static int len(int x){
if(x<10) return 1;
return len(x/10)+1;
}
<p>
// 取x的第k位数字
static int f(int x, int k){
if(len(x)-k==0) return x%10;
return ______________________;  //填空
}
<p>
public static void main(String[] args)
{
int x = 23513;
//System.out.println(len(x));
System.out.println(f(x,3));
}
}

对于题目中的测试数据，应该打印5。

请仔细分析源码，并补充划线部分所缺少的代码。

注意：只提交缺失的代码，不要填写任何已有内容或说明性的文字。
 ```
 从后往前删除数字
 ```
 public class E {
    static int len(int x) {
        if (x < 10) return 1;
        return len(x / 10) + 1;
    }

    // 取x的第k位数字
    static int f(int x, int k) {
        if (len(x) - k == 0) return x % 10;
        return f(x/10,k);  //填空
    }

    public static void main(String[] args) {
        int x = 23513;
        //System.out.println(len(x));
        System.out.println(f(x, 3));
    }
}
 ```
 
 ## 第六题
 
 
 最长上升子序列
 ```
 public class F {
    static int f(String s1, String s2) {
        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();

        int[][] a = new int[c1.length + 1][c2.length + 1];

        int max = 0;
        for (int i = 1; i < a.length; i++) {
            for (int j = 1; j < a[i].length; j++) {
                if (c1[i - 1] == c2[j - 1]) {
                    a[i][j] = a[i-1][j-1] + 1;  //填空
                    if (a[i][j] > max) max = a[i][j];
                }
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int n = f("abcdkkk", "baabcdadabc");
        System.out.println(n);
    }
}
 ```
 
 ## 第七题

标题：日期问题

小明正在整理一批历史文献。这些历史文献中出现了很多日期。小明知道这些日期都在1960年1月1日至2059年12月31日。令小明头疼的是，这些日期采用的格式非常不统一，有采用年/月/日的，有采用月/日/年的，还有采用日/月/年的。更加麻烦的是，年份也都省略了前两位，使得文献上的一个日期，存在很多可能的日期与其对应。  

比如02/03/04，可能是2002年03月04日、2004年02月03日或2004年03月02日。  

给出一个文献上的日期，你能帮助小明判断有哪些可能的日期对其对应吗？

输入

一个日期，格式是"AA/BB/CC"。  (0 <= A, B, C <= 9)  

输入

输出若干个不相同的日期，每个日期一行，格式是"yyyy-MM-dd"。多个日期按从早到晚排列。  

样例输入

02/03/04  

样例输出

2002-03-04  
2004-02-03  
2004-03-02  

资源约定：
峰值内存消耗（含虚拟机） < 256M
CPU消耗  < 1000ms


请严格按要求输出，不要画蛇添足地打印类似：“请您输入...” 的多余内容。

所有代码放在同一个源文件中，调试通过后，拷贝提交该源码。
不要使用package语句。不要使用jdk1.7及以上版本的特性。
主类的名字必须是：Main，否则按无效代码处理。


处理时间比较麻烦
```
import java.util.Arrays;
import java.util.Scanner;

public class G {

    private static int[] m = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        String tmp = cin.nextLine();
        String s[] = tmp.split("/");
        int a = Integer.parseInt(s[0]);
        int b = Integer.parseInt(s[1]);
        int c = Integer.parseInt(s[2]);

        int sum[] = new int[6];
        sum[0] = (1900 + a) * 10000 + b * 100 + c;
        sum[1] = (1900 + c) * 10000 + a * 100 + b;
        sum[2] = (1900 + c) * 10000 + b * 100 + a;
        sum[3] = (2000 + a) * 10000 + b * 100 + c;
        sum[4] = (2000 + c) * 10000 + a * 100 + b;
        sum[5] = (2000 + c) * 10000 + b * 100 + a;
        Arrays.sort(sum);

        for (int i = 0; i < sum.length; i++) {
            int year = sum[i] / 10000;
            int month = (sum[i] % 10000) / 100;
            int day = sum[i] % 100;
            if (i != 0 && sum[i] == sum[i - 1]) continue;
            if (year >= 1960 && year <= 2059) {
                if (mm(year)) {
                    m[1] = 29;
                } else {
                    m[1] = 28;
                }
                if (month >= 1 && month <= 12) {
                    if (day >= 1 && day <= m[month - 1]) {
                        String f = sum[i] + "";
                        System.out.println(f.substring(0, 4) + "-" + f.substring(4, 6) + "-" + f.substring(6, 8));
                    }
                }
            }
        }


    }

    private static boolean mm(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            return true;
        } else return false;
    }

}

```


## 第八题
标题：包子凑数

小明几乎每天早晨都会在一家包子铺吃早餐。他发现这家包子铺有N种蒸笼，其中第i种蒸笼恰好能放Ai个包子。每种蒸笼都有非常多笼，可以认为是无限笼。

每当有顾客想买X个包子，卖包子的大叔就会迅速选出若干笼包子来，使得这若干笼中恰好一共有X个包子。比如一共有3种蒸笼，分别能放3、4和5个包子。当顾客想买11个包子时，大叔就会选2笼3个的再加1笼5个的（也可能选出1笼3个的再加2笼4个的）。

当然有时包子大叔无论如何也凑不出顾客想买的数量。比如一共有3种蒸笼，分别能放4、5和6个包子。而顾客想买7个包子时，大叔就凑不出来了。

小明想知道一共有多少种数目是包子大叔凑不出来的。

输入

第一行包含一个整数N。(1 <= N <= 100)
以下N行每行包含一个整数Ai。(1 <= Ai <= 100)  

输出

一个整数代表答案。如果凑不出的数目有无限多个，输出INF。

例如，
输入：
2  
4  
5   

程序应该输出：
6  

再例如，
输入：
2  
4  
6    

程序应该输出：
INF

样例解释：
对于样例1，凑不出的数目包括：1, 2, 3, 6, 7, 11。  
对于样例2，所有奇数都凑不出来，所以有无限多个。  

资源约定：
峰值内存消耗（含虚拟机） < 256M
CPU消耗  < 1000ms


请严格按要求输出，不要画蛇添足地打印类似：“请您输入...” 的多余内容。

所有代码放在同一个源文件中，调试通过后，拷贝提交该源码。
不要使用package语句。不要使用jdk1.7及以上版本的特性。
主类的名字必须是：Main，否则按无效代码处理。
提交程序时，注意选择所期望的语言类型和编译器类型。

先求所有数的公约数，如果是1的话就可以凑出有限个，否则是无限个  
在使用dp求有限个的个数
dp[i] = 1 是能组成i个包子的情况
if dp[i-a[j]] == 1 then dp[i] = 1


```
public class H {
    private static int a[] = new int[100 + 10];
    private static int dp[] = new int[10000 + 10];


    public static void main(String[] args) {
        int n;
        Scanner cin = new Scanner(System.in);
        n = cin.nextInt();
        for (int i = 0; i < n; i++) {
            a[i] = cin.nextInt();
        }
        int flag = 0;
        int p = a[0];
        for (int i = 1; i < n; i++) {
            p = gcd(p, a[i]);
            if (p == 1) {
                flag = 1;
                break;
            }
        }
        if (flag != 1) {
            System.out.println("INF");
            return;
        }
        dp[0] = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < 10000; j++) {
                if (a[i] > j) continue;
                if (dp[j - a[i]] == 1) dp[j] = 1;
            }
        }
        int res = 0;
        for (int i = 0; i < 10000; i++) {
            if (dp[i] != 1) res++;
        }
        System.out.println(res);
    }

    private static int gcd(int a, int b) {
        if (b == 0) return a;
        else return gcd(b, a % b);
    }
}
```

##第九题


标题： 分巧克力

    儿童节那天有K位小朋友到小明家做客。小明拿出了珍藏的巧克力招待小朋友们。
    小明一共有N块巧克力，其中第i块是Hi x Wi的方格组成的长方形。

    为了公平起见，小明需要从这 N 块巧克力中切出K块巧克力分给小朋友们。切出的巧克力需要满足：

    1. 形状是正方形，边长是整数  
    2. 大小相同  

例如一块6x5的巧克力可以切出6块2x2的巧克力或者2块3x3的巧克力。

当然小朋友们都希望得到的巧克力尽可能大，你能帮小Hi计算出最大的边长是多少么？

输入
第一行包含两个整数N和K。(1 <= N, K <= 100000)  
以下N行每行包含两个整数Hi和Wi。(1 <= Hi, Wi <= 100000) 
输入保证每位小朋友至少能获得一块1x1的巧克力。   

输出
输出切出的正方形巧克力最大可能的边长。

样例输入：
2 10  
6 5  
5 6  

样例输出：
2

资源约定：
峰值内存消耗（含虚拟机） < 256M
CPU消耗  < 1000ms


请严格按要求输出，不要画蛇添足地打印类似：“请您输入...” 的多余内容。

所有代码放在同一个源文件中，调试通过后，拷贝提交该源码。
不要使用package语句。不要使用jdk1.7及以上版本的特性。
主类的名字必须是：Main，否则按无效代码处理。


明显二分题目，因为数据量小，注意二分的次数，2^n 能大于最大的数就可以，

```
public class I {
    private static Scanner cin = new Scanner(new BufferedInputStream(System.in));
    private static int w[] = new int[100000+10];
    private static int h[] = new int[100000+10];
    public static void main(String[] args) {
        run();
    }

    private static void run(){
        int n,k;
        n = cin.nextInt();
        k=cin.nextInt();
        ;
        for(int i=0;i<n;i++){
            h[i] = cin.nextInt();
            w[i] = cin.nextInt();
        }
        int l=0,r=100000;
        int mid = 0;
        for(int i=0;i<20;i++){
            mid = (r+l)/2;
            int p = judge(n,mid);
            if(p >= k){
                l=mid;
            }else {
                r = mid;
            }
        }
        System.out.println(mid);
    }

    private static int judge(int n,int x){
        int sum=0;
        for(int i=0;i<n;i++){
            int z1 = h[i]/x;
            int z2 = w[i]/x;
            sum += z1*z2;
        }
        return sum;
    }
}
```
