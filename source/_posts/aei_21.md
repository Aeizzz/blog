---
title: django完成blog
date: 2018-02-20 22:40:20
categories: python
tags: django
---
使用django完成一个简单的blog，练习python web开发,和django  
本文学习 [追梦人物的博客](https://www.zmrenwu.com/post/2/)

主要说一些编写代码过程中遇到的难点和重点

编写的编写的代码放在的github上 [blog_django](https://github.com/hong-ll/blog_django)
## 支持markdown语法和代码高亮
### 安装markdown
首先安装markdown，`pip install markdown`
### 渲染markdown
再detail视图中渲染markdown,如下
```python
def detail(request,pk):
    post = get_object_or_404(Post,pk=pk)
    post.body = markdown.markdown(post.body,
                                extensions=[
                                    'markdown.extensions.extra',
                                    'markdown.extensions.codehilite',
                                    'markdown.extensions.toc',
                                ])
    return render(request,'blog/detail.html',context={'post':post})
```
这样我们在模板中展示 {{ post.body }} 的时候，就不再是原始的 Markdown 文本了，而是渲染过后的 HTML 文本。注意这里我们给 markdown 渲染函数传递了额外的参数 extensions，它是对 Markdown 语法的拓展，这里我们使用了三个拓展，分别是 extra、codehilite、toc。extra 本身包含很多拓展，而 codehilite 是语法高亮拓展，这为我们后面的实现代码高亮功能提供基础，而 toc 则允许我们自动生成目录

### 使用safe 标签  
我们在发布的文章详情页没有看到预期的效果，而是类似于一堆乱码一样的 HTML 标签，这些标签本应该在浏览器显示它本身的格式，但是 Django 出于安全方面的考虑，任何的 HTML 代码在 Django 的模板中都会被转义（即显示原始的 HTML 代码，而不是经浏览器渲染后的格式）。为了解除转义，只需在模板标签使用 safe 过滤器即可，告诉 Django，这段文本是安全的，你什么也不用做。在模板中找到展示博客文章主体的 {{ post.body }} 部分，为其加上 safe 过滤器，{{ post.body|safe }}，大功告成，这下看到预期效果了。

## 使用自定义标签
### 模板标签目录结构
首先在我们的 blog 应用下创建一个 templatetags 文件夹。然后在这个文件夹下创建一个 __init__.py 文件，使这个文件夹成为一个 Python 包，之后在 templatetags\目录下创建一个 blog_tags.py 文件，这个文件存放自定义的模板标签代码。

### 编写模板标签代码  
接下来就是编写各个模板标签的代码了，自定义模板标签代码写在 blog_tags.py 文件中。其实模板标签本质上就是一个 Python 函数，因此按照 Python函数的思路来编写模板标签的代码就可以了，并没有任何新奇的东西或者需要新学习的知识在里面。

### 最新文章模板标签  
```python
blog/templatetags/blog_tags.py
from ..models import Post
def get_recent_posts(num=5):
     return Post.objects.all().order_by('-created_time')[:num]
```  

这个函数的功能是获取数据库中前 num 篇文章，这里 num 默认为 5。函数就这么简单，但目前它还只是一个纯 Python 函数，Django 在模板中还不知道该如何使用它。为了能够通过 get_recent_posts 的语法在模板中调用这个函数，必须按照 Django 的规定注册这个函数为模板标签，方法如下：

```python
blog/templatetags/blog_tags.py

from django import template
from ..models import Post

register = template.Library()

@register.simple_tag
def get_recent_posts(num=5):
    return Post.objects.all().order_by('-created_time')[:num]
```
这里我们首先导入 template 这个模块，然后实例化了一个 template.Library 类，并将函数get_recent_posts 装饰为 register.simple_tag.这样就可以在模板中使用语法 `get_recent_posts` 调用这个函数了。

#### 使用自定义的模板标签  
打开 base.html，为了使用模板标签，我们首先需要在模板中导入存放这些模板标签的模块，这里是 blog_tags.py 模块。当时我们为了使用 static 模板标签时曾经导入过 `load staticfiles`，这次在 `load staticfiles` 下再导入 blog_tags：

```html
templates/base.html

{% load staticfiles %}
{% load blog_tags %} 
<!DOCTYPE html>
<html>
...
</html>
```