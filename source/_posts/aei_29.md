---
title: MySQL创建用户与授权
date: 2018-03-26 19:02:38
categories: mysql
tags: mysql
top: false
---
## 创建用户
命令 ：
```
CREATE USER 'username'@'host' IDENTIFIED BY 'password';
```
说明：
 - username：将要创建的用户名
 - host：指定该用户在那个主机上可以登录，如果是本地用户可用localhost，如果任意则可以使用通配符%
 - password: 该用户的登录密码，密码可以为空，如果为空则该用户可以不需要密码登陆服务器
 
例子：
```
CREATE USER 'dog'@'localhost' IDENTIFIED BY '123456';
CREATE USER 'pig'@'192.168.1.101_' IDENDIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '';
CREATE USER 'pig'@'%';
```
## 授权
命令：
```
GRANT privileges ON databasename.tablename TO 'username'@'host'
```
说明：
 - privileges：用户的操作权限，如SELECT，INSERT，UPDATE等，如果要授予所的权限则使用ALL
 - databasename：数据库名
 - tablename：表名，如果要授予该用户对所有数据库和表的相应操作权限则可用`*`表示，如`*.*`
 
注意
用以上命令授权的用户不能给其它用户授权，如果想让该用户可以授权，用以下命令:
```
GRANT privileges ON databasename.tablename TO 'username'@'host' WITH GRANT OPTION;
```
## 设置与更改用户密码
命令：
```
SET PASSWORD FOR 'username'@'host' = PASSWORD('newpassword');
```
如果是当前登陆用户用:
```
SET PASSWORD = PASSWORD("newpassword");
```
## 撤销用户权限
命令:
```
REVOKE privilege ON databasename.tablename FROM 'username'@'host';
```
说明:
privilege, databasename, tablename：同授权部分
## 删除用户
命令:
DROP USER 'username'@'host';