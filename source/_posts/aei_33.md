---
title: 'Two Sum [leetcode]'
date: 2018-02-14 22:13:05
categories: Algorithm
tags: leetcode
---

## 题目
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

## 题解
给出列表，找出列表nums中的随意两个的和等于target

一开始使用两个for，超时。。。。我觉的c++的话不会超时

后来则改为hash的使用，python则使用dict就可以了

代码很简单，每次拿出一个数，看另一个数时候已经出现，如果出现过则完事，没有的话就把当前的数加进字典中

## 代码
 - python版本
```
class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        g = []
        data = dict()
        for i in range(len(nums)):
            p = target - nums[i]
            if p in data:
                g.append(i)
                g.append(data[p])
                return g
            data[nums[i]] = i
        return g
```

