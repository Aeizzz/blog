---
title: tensorflow
date: 2018-01-19 19:53:37
categories: tensorflow
tags: tensorflow
---
## tensorflow安装
使用pip安装，另外使用豆瓣源
```
pip install tensorflow -i https://pypi.douban.com/simple
```


## 坑
如果你使用 tf 时出现这个
```
Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX AVX2
```
那就在文件中加入
```
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
```
# 学习


2-1
```
# -*- coding: utf-8 -*-
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf
# 创建矩阵
m1 = tf.constant([[3,3]])
m2 = tf.constant([[2],[3]])
# 矩阵相乘
product = tf.matmul(m1,m2)
print(product)
# 结果 Tensor("MatMul:0", shape=(1, 1), dtype=int32)

## 定义一个会话，启动默认图

sess = tf.Session()

result = sess.run(product)
print(result)
sess.close()

# 也可以采用这种方法，不需要关闭会话
with tf.Session() as sess:
    result = sess.run(product)
    print(result)
```

运行结果
```
Tensor("MatMul:0", shape=(1, 1), dtype=int32)
[[15]]
[[15]]
```


2-2
```
# -*- coding: utf-8 -*-

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
x = tf.Variable([1,2])
a = tf.constant([2,3])
# 增加一个减法op
sub = tf.subtract(x,a)
# 增加一个加法op
add = tf.add(x,a)

# 初始化
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    print(sess.run(sub))
    print(sess.run(add))

## 创建一个变量初始化为0
state = tf.Variable(0,name='counter')
## 创建一个加法op，作用state加1
new_value = tf.add(state,1)
## 赋值，后面赋值给前面，不能用等号
update = tf.assign(state,new_value)
init = tf.global_variables_initializer()

# 循环
with tf.Session() as sess:
    sess.run(init)
    print(sess.run(state))
    for _ in range(5):
        sess.run(update)
        print(sess.run(state))
```

结果
```
[-1 -1]
[3 5]
0
1
2
3
4
5
```

2-3
```
# -*- coding: utf-8 -*-


import tensorflow as tf

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


## Fetch  同时运算
input1 = tf.constant(3.0)
input2 = tf.constant(2.0)
input3 = tf.constant(5.0)

add = tf.add(input2,input3)
mul = tf.multiply(input1,add)

with tf.Session() as sess:
    result = sess.run([mul,add])
    print(result)


## Feed
# 创建占位符
input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)

output = tf.multiply(input1,input2)

with tf.Session() as sess:
    # feed的数据以字典的形式传入
    print(sess.run(output,feed_dict={input1:[7.],input2:[2.]}))
```
结果
```
[21.0, 7.0]
[ 14.]
```

## 一个线性模型
```
# -*- coding: utf-8 -*-



import os

import numpy as np
## 简单的使用示例
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# 使用numpy生成100个随机的点
x_data = np.random.rand(100)
y_data = x_data * 0.1 + 0.2
##构造一个线性模型
b = tf.Variable(0.)
k = tf.Variable(0.)
y = k*x_data + b
# 二次代价函数
'''
reduce_mean平均值
square平方
'''
loss = tf.reduce_mean(tf.square(y_data-y))
# 定义一个梯度下降法进行训练的优化器
optimizer = tf.train.GradientDescentOptimizer(0.2)
# 最小化代价函数
train = optimizer.minimize(loss)
# 初始化变量
init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    for step in range(201):
        sess.run(train)
        if step%20 == 0:
            print(step,sess.run([k,b]))




```
结果
```
0 [0.055125017, 0.10061224]
20 [0.10449799, 0.19752398]
40 [0.10268082, 0.19852433]
60 [0.10159777, 0.19912049]
80 [0.10095227, 0.19947581]
100 [0.10056755, 0.19968759]
120 [0.10033826, 0.19981381]
140 [0.10020161, 0.19988902]
160 [0.10012015, 0.19993386]
180 [0.10007161, 0.19996057]
200 [0.10004269, 0.19997649]
```
可以看出左后k和b接近0.1和0.2
