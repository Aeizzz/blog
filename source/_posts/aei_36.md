---
title: ubuntu 16.04 安装深度学习环境
date: 2018-03-17 08:20:27
categories: 深度学习
tags: 装机
top: false
---
安装深度学习环境主要有
 - 装机
 - 安装驱动
 - 安装CUDA
 - cnDNN
 - Anaconda
 - python
 - Tensorflow
 ---
## 装机
推荐清华大学源下载镜像，我使用的是ubuntu 16.04 桌面版，其实使用server版更好  
[ 清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)
安装系统过程就不写了，使用U盘安装很简单

---
## 安装驱动
 我使用的是nvidia 1070 显卡
 1. 从nvidia官网查询自己的显卡所要安装的型号
![](https://i.loli.net/2018/03/17/5aac61e56cd3b.png)
可以看出是391型号
 2. 添加源
 `sudo add-apt-repository ppa:graphics-drivers/ppa`
 3. 安装驱动
 `sudo apt-get update && sudo apt-get install nvidia-391`
 当然如果原来有驱动的话则先拆卸
 `sudo apt-get purge nvidia*`
 安装完成后
 `nvidia-smi`
 查看驱动信息
 ![](https://i.loli.net/2018/03/17/5aac672fee519.png)
 
---
## 安装CUDA
首先在官网CUDA(https://developer.nvidia.com/cuda-downloads)下载相应的CUDA版本
![](https://i.loli.net/2018/03/17/5aac7755830f3.png)
使用wget下载
`wget https://developer.nvidia.com/compute/cuda/9.1/Prod/local_installers/cuda_9.1.85_387.26_linux`
下载后然后运行  
设置环境变量
`vim ~/.bashrc`
在文本后面添加，根据自己的目录更改
```
export PATH=/usr/local/cuda-8.0/bin:$PATH  
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64:$LD_LIBRARY_PATH
export CUDA_HOME=/usr/local/cuda
```
```
环境变量立即生效 
source ~/.bashrc  
```
检车CUDA是否安装成功
```
nvcc --version
```
![](https://i.loli.net/2018/03/17/5aac7768efb26.png)

---

## 安装cnDNN
在官网下载cuDNN(https://developer.nvidia.com/rdp/cudnn-download),需要注册一个账号
![](https://i.loli.net/2018/03/17/5aac776923405.png)
选择自己要用的
![](https://i.loli.net/2018/03/17/5aac776921c26.png)

安装cuDNN

```
#解压文件
tar -zxvf cudnn-9.0-linux-x64-v7.tgz

#切换到刚刚解压出来的文件夹路径
cd cuda 
#复制include里的头文件（记得转到include文件里执行下面命令）
sudo cp /include/cudnn.h  /usr/local/cuda/include/

#复制lib64下的lib文件到cuda安装路径下的lib64（记得转到lib64文件里执行下面命令）
sudo cp lib*  /usr/local/cuda/lib64/

#设置权限
sudo chmod a+r /usr/local/cuda/include/cudnn.h 
sudo chmod a+r /usr/local/cuda/lib64/libcudnn*

#======更新软连接======
cd /usr/local/cuda/lib64/ 
sudo rm -rf libcudnn.so libcudnn.so.7   #删除原有动态文件，版本号注意变化，可在cudnn的lib64文件夹中查看   
sudo ln -s libcudnn.so.7.0.2 libcudnn.so.7  #生成软衔接（注意这里要和自己下载的cudnn版本对应，可以在/usr/local/cuda/lib64下查看自己libcudnn的版本）
sudo ln -s libcudnn.so.7 libcudnn.so #生成软链接
sudo ldconfig -v #立刻生效
```
---

## 安装anaconda
同样选择清华大学源
`wget https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/Anaconda3-5.1.0-Linux-x86_64.sh`
下载完直接安装
安装完刷新变量`source ~/.bashrc`

创建一个虚拟环境  
`conda create -n tensorflow python=2.7`  
激活conda环境  
`source activate tensorflow`

## 安装tensorflow
在某个虚拟环境中直接安装就可以了
`pip install tensorflow`