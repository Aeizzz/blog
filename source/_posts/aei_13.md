---
title: 在博客上使用gitment评论
date: 2018-01-05 13:28:29
categories: 其他
tags: gitment评论
---
多说倒闭，disqus被墙，国内没有太好的评论系统。<br>
然后想把服务器弄成国外的vps时发现了一个好用的评论系统[gitment](https://github.com/imsun/gitment)<br>

Gitment 是作者实现的一款基于 GitHub Issues 的评论系统。支持在前端直接引入，不需要任何后端代码。可以在页面进行登录、查看、评论、点赞等操作，同时有完整的 Markdown / GFM 和代码高亮支持。尤为适合各种基于 GitHub Pages 的静态博客或项目页面。

下面就写一下使用教程
###  申请一个Github OAuth Application
[点击此处](https://github.com/settings/applications/new) 来注册一个新的 OAuth Application。其他内容可以随意填写，但要确保填入正确的 callback URL（一般是评论页面对应的域名，如本站 http://blog.hong-ll.xyz/ ）。

你会得到一个 client ID 和一个 client secret，这个将被用于之后的用户登录。

### 引入 Gitment
将下面的代码添加到你的页面：
```
<div id="container"></div>
<link rel="stylesheet" href="https://imsun.github.io/gitment/style/default.css">
<script src="https://imsun.github.io/gitment/dist/gitment.browser.js"></script>
<script>
var gitment = new Gitment({
  id: '页面 ID', // 可选。默认为 location.href
  owner: '你的 GitHub ID',
  repo: '存储评论的 repo',
  oauth: {
    client_id: '你的 client ID',
    client_secret: '你的 client secret',
  },
})
gitment.render('container')
</script>
```
其中id可以不用写,下面放上我的代码用做示范
```
<div id="container"></div>
<link rel="stylesheet" href="https://imsun.github.io/gitment/style/default.css">
<script src="https://imsun.github.io/gitment/dist/gitment.browser.js"></script>
<script>
var gitment = new Gitment({
  owner: 'hong-ll',//这里写自己的github用户名
  repo: 'file',//这里写仓库的名字，一开始写仓库的地址总是有问题
  oauth: {  //下面两个就不用说了，刚才申请给的，应该不会填错
    client_id: '**********',
    client_secret: '**********',
  },
})
gitment.render('container')
</script>
```

###  初始化评论
每一篇新的博客都要自己登录自己的github账号，然后点击`Initialize Comments`
如下图
![](https://github.com/hong-ll/file/blob/master/QQ%E6%88%AA%E5%9B%BE20170811153814.png?raw=true)

---
以上就这么多了
自己也可以去项目的github上看看，都很全



