---
title: tensorflow-张量
date: 2018-03-24 21:00:00
categories: 深度学习
tags: 
 - tensorflow
top: false
---
# 张量的概念
在tensorflow中所有的数据都是使用张量来表示，从功能上讲张量可以被简单理解为多维数组
其中零张量表示标量，也就是一个数。
第一张量为向量(vector),也就是一维数组。
第n张量可以理解为一个n维数组。
但是张量在tensorflow中的实现并不是直接采用数组的形式，它只是对TensorFlow中运算的引用。在张量中并没有真正的保存数字，他保存的是如何得到这些数字的计算过程。
以张量加法为例，如下代码，并不会得到加法的结果，而会得到对结果的引用
```
import tensorflow as tf
# tf.constant 是一个计算，这个计算的结果是一个张量，保存在变量a中
a = tf.constant([1.0,2.0],name='a')
b = tf.constant([2.0,3.0],name='b')

result = tf.add(a,b,name='add')
print(result)
-------
输出
Tensor("add:0", shape=(2,), dtype=float32)
```

从上面的代码中可以看出TensorFlow计算的结果不是一个具体的数字，而是一个张量的结构。
一个张量主要保存了三个属性:名字(name),维度(shape)和类型(type)

张量的属性名字不仅是一个张量的唯一标识，他同样也给出了这个张量是如何计算出来的。张量的命名可以通过`node:src_output`的形式给出。其中node为节点的名称，src_output表示当前张量来自节点的第几个输出

张量的维度(shape)描述了一个张量的维度信息，比如上面样例中 shape=(2,) 说明了张量restult是一个一维数组，这个数组长度为2，维度是张量一个很重要的属性

张量的第三个属性是类型(type),每一张量会有一个唯一的类型，Tensorflow会对参与运算的所有张量进行类型的检查，发现类型不匹配是会报错

TensorFlow支持14中不同的类型，主要包括师叔(tf.float32,tf.float64),整数(tf.int8,tf.int16,tf.int32,tf.int64,tf.uint8),布尔型(tf.bool)和复数(tf.complex64,tf.complex128)


