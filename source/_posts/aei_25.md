---
title: hexo文章加密
date: 2018-01-09 00:30:58
categories: 其他
tags: hexo
password: password
---
使文章加密的教程，从别处看来的
文章密码：password
<!-- more -->
转载连接[Hexo文章简单加密访问](https://www.jianshu.com/p/a2330937de6c)

只需要两步
 1. 修改文件
 把一下代码添加到`themes->next->layout->_partials->head.swig`中
 ```
 <script>
    (function(){
        if('{{ page.password }}'){
            if (prompt('请输入文章密码') !== '{{ page.password }}'){
                alert('密码错误！');
                history.back();
            }
        }
    })();
</script>
 ```
 2. 给你想要加密的文章添加头部，password这个属性
 如
 ```
 ---
title: Hexo文章简单加密访问
date: 2016-12-01 10:45:29
tags: hexo
categories: 博客
keywords:
    - Hexo
    - 加密
description: 文章访问密码：password
password: password
---
 ```