---
title: 自动化部署搭建过程一
date: 2018-03-27 16:45:18
categories: 自动化部署
tags: gitlab
top: false
---
# 首先搭建gitlab
## 系统选择
我选择的系统是ubuntu 16.04
环境是docker 1.13.1
```
Docker version 1.13.1, build 092cba3
```
docker-compose版本是1.8.0
```
docker-compose version 1.8.0, build unknown
```
## 安装环境
#### 更改ubuntu系统源
我使用清华大学源
Ubuntu 的软件源配置文件是 /etc/apt/sources.list。将系统自带的该文件做个备份，将该文件替换为下面内容，即可使用 TUNA 的软件源镜像。
```
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-security main restricted universe multiverse

# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-proposed main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-proposed main restricted universe multiverse
```
使用 `apt update` 更新
#### 安装docker && gitlab
 - 安装docker 和 docker-compose
 ```
 sudo apt update
 sudo apt install docker.io
 sudo apt install docker-compose
 ```
 - 使用docker-compose.yml安装gitlab
 文件内容为
```
web:
  image: 'gitlab/gitlab-ce:latest'
  restart: always
  hostname: 'gitlab.7326it.club'
  environment:
    GITLAB_OMNIBUS_CONFIG: |
      external_url 'http://gitlab.7326it.club'
      gitlab_rails['gitlab_shell_ssh_port'] = 2222
      # Add any other gitlab.rb configuration here, each on its own line
  ports:
    - '0.0.0.0:80:80'
    - '0.0.0.0:443:443'
    - '0.0.0.0:2222:22'
  volumes:
    - '/home/gitlab/gitlab/config:/etc/gitlab'
    - '/home/gitlab/gitlab/logs:/var/log/gitlab'
    - '/home/gitlab/gitlab/data:/var/opt/gitlab'
```
 其中自行更改
hostname:注意名，一般填域名，没有域名的话填ip也是可以的
external_url: 访问的链接,同上面一样
gitlab_rails['gitlab_shell_ssh_port'] = 2222: 配置ssh，2222就是使用ssh链接的端口
ports:这个是需要开放的端口
volumes:这个是映射到宿主机的目录，因为容器中不存放数据，防止丢失
 - 启动
```
在docker-compose.yml所在目录运行
docker-compose up -d
```
几分钟后访问地址就可以看到gitlab可以使用了
