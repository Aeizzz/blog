---
title: 自动化部署搭建过程五
date: 2018-03-28 18:28:00
categories: 自动化部署
tags: 自动化构建成功
top: false
---
 - 在gitlab上创建一个空项目
 - 使用idea创建一个spring-boot项目，并配置好maven
 - 然后连接我们的仓库
 ```
git init
git remote add origin http://210.44.71.43/root/test.git
git add .
git commit -m "Initial commit"
git push -u origin master
 ```
这样就有可以把自己的项目push到远程仓库

 - 配置jenkins
  - 新建一个maven项目
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328070304030.png)
  - 在源码构建中选择Git
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328070409081.png)
贴上仓库的地址如果需要用户名则自己创建一个用户名
  - 构建触发器选择
 ![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328070540304.png)
 使用gitlab
 
   - 切到gitlab项目中， setting > integrations 下有个填写URL和Token的地方
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328070714924.png)
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328070755771.png)
分别将这两个粘贴到gitlab中

 - Build中
因为使用的maven，并且会直接push docker images
所以我们填 `clean package docker:build -DpushImage -e -X`
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328070952811.png)

 - 最后保存

---
坑  
在构建的时候回出现各种错误,我们要会更具错误去排查，从而解决错误
贴几个我出现的错误
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328071200256.png)
这个错误主要是使用docker registry是要把本机的docker配置文件更改，不然回强制使用https，关于怎么更改看安装docker registry那一个
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328071329119.png)
这个错误是链接不到docker  不是生成docker镜像，我选择的方法是将自己本机的docker 改成可以远程链接，可以解决
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328071439693.png)
这一个错误是生成的docker images的名称不对，docker 的name不能有`-`等特殊字符
![](http://olz5306lf.bkt.clouddn.com/20180328182800/20180328071647048.png)
这个错误还没有解决，但是并不影响我们构建