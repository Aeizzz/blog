---
title: pinpoint 搭建使用和常见错误
date: 2018-05-29 15:57:28
categories: 自动化部署
tags: pinpoint
top: false
---
## pinpoint介绍
Pinpoint是一个开源的 APM (Application Performance Management/应用性能管理)工具，用于基于java的大规模分布式系统。
仿照 Google Dapper , Pinpoint 通过跟踪分布式应用之间的调用来提供解决方案，以帮助分析系统的总体结构和内部模块之间如何相互联系.

    注：对于各个模块之间的通讯英文原文中用的是transaction一词，但是我觉得如果翻译为"事务"容易引起误解，所以替换为"交互"或者"调用"这种比较直白的字眼。

在使用上力图简单高效：

 - 安装agent，不需要修改哪怕一行代码
 - 最小化性能损失

---

## pinpoint的搭建
准备工作
```
hbase-1.2.6-bin.tar.gz 
pinpoint-collector-1.7.1.war        collector模块，使用tomcat进行部署
pinpoint-web-1.7.1.war              web管控台，使用tomcat进行部署
pinpoint-agent-1.7.1.tar.gz         不需要部署，在被监控应用一端
apache-tomcat-8.5.29.tar.gz
jdk-8u171-linux-x64.tar.gz
```
准备两个tomcat环境，一个部署`pinpoint-collector-1.7.1.war` ，一个部署`pinpoint-web-1.7.1.war`  

首先在用户根目录创建pp_res文件夹把所需要的文件复制进去
```
mkdir /home/pp_res
cd /home/pp_res/
```

查看上传时候成功
```
[root@ecs-6707 pp_res]# ll
total 430724
-rw-r--r-- 1 root root   9532698 May 28 15:57 apache-tomcat-8.5.29.tar.gz
-rw-r--r-- 1 root root 104659474 May 28 15:59 hbase-1.2.6-bin.tar.gz
-rw-r--r-- 1 root root     15763 May 28 15:59 hbase-create.hbase
-rw-r--r-- 1 root root 190890122 May 28 16:33 jdk-8u171-linux-x64.tar.gz
-rw-r--r-- 1 root root   8883403 May 28 17:01 pinpoint-agent-1.7.1.tar.gz
-rw-r--r-- 1 root root  48335392 May 28 16:01 pinpoint-collector-1.7.1.war
-rw-r--r-- 1 root root  78732778 May 28 16:01 pinpoint-web-1.7.1.war
```

### 安装jdk
安装jdk8 
```
cd /home/pp_res/
tar -zxvf jdk-8u171-linux-x64.tar.gz
mkdir /usr/java
mv jdk1.8.5_29/ /usr/java/jdk18
```
配置环境变量
```
vi /etc/profile

export JAVA_HOME=/usr/java/jdk18
export PATH=$PATH:$JAVA_HOME/bin
```
让环境变量生效
```
source /etc/profile
```


### 安装HBASE
pinpoint收集来的测试数据，主要是存在Hbase数据库的。所以它可以收集大量的数据，可以进行更加详细的分析。

将 Hbase解压，并放入制定目录
```
cd /home/pp_res/
tar -zxvf hbase-1.2.6-bin.tar.gz 
mkdir -p /data/service
mv hbase-1.2.6/ /data/service/hbase
```

修改hbase-env.sh的JAVA_HOME环境变量位置
```
cd /data/service/hbase/conf/
vi hbase-env.sh

增加
export JAVA_HOME=/usr/java/jdk18/
```
修改Hbase的配置信息
```
<configuration>
    <property>
        <name>hbase.rootdir</name>
        <value>file:///pinpoint/data/hbase</value>
    </property>
    <property>
        <name>hbase.zookeeper.property.dataDir</name>
        <value>/pinpoint/data/zookeeper</value>
    </property>
    <property>
        <name>hbase.master.port</name>
        <value>60000</value>
    </property>
    <property>
        <name>hbase.regionserver.port</name>
        <value>60020</value>
    </property>
</configuration>
```
启动 hbase
```
cd /data/service/hbase/bin
./start-hbase.sh
```
初始化Hbase的pinpoint库
```
./hbase shell /home/pp_res/hbase-create.hbase
```
执行完以后进入hbase,查看表是否存在
```
./hbase shell


[root@ecs-6707 bin]# ./hbase shell
2018-05-29 16:42:02,459 WARN  [main] util.NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
HBase Shell; enter 'help<RETURN>' for list of supported commands.
Type "exit<RETURN>" to leave the HBase Shell
Version 1.2.6, rUnknown, Mon May 29 02:25:32 CDT 2017

hbase(main):001:0> list
TABLE
AgentEvent
AgentInfo
AgentLifeCycle
AgentStatV2
ApiMetaData
ApplicationIndex
ApplicationMapStatisticsCallee_Ver2
ApplicationMapStatisticsCaller_Ver2
ApplicationMapStatisticsSelf_Ver2
ApplicationStatAggre
ApplicationTraceIndex
HostApplicationMap_Ver2
SqlMetaData_Ver2
StringMetaData
TraceV2
15 row(s) in 0.3470 seconds

=> ["AgentEvent", "AgentInfo", "AgentLifeCycle", "AgentStatV2", "ApiMetaData", "ApplicationIndex", "ApplicationMapStatisticsCallee_Ver2", "ApplicationMapStatisticsCaller_Ver2", "ApplicationMapStatisticsSelf_Ver2", "ApplicationStatAggre", "ApplicationTraceIndex", "HostApplicationMap_Ver2", "SqlMetaData_Ver2", "StringMetaData", "TraceV2"]
hbase(main):002:0>

```
### 安装pinpoint-collector
部署war包  
解压Tomcat，将Tomcat重命名移动到指定位置
```
cd /home/pp_res/
tar -zxvf apache-tomcat-8.5.29.tar.gz
mv apache-tomcat-8.5.29/ /data/service/pp-col
```
修改pp-col的Tomcat的配置，主要修改端口，避免与pp-web的Tomcat的端口冲突。我在原本默认的端口前都加了1，下面是替换的shell命令。
```
cd /data/service/pp-col/conf/
sed -i 's/port="8005"/port="18005"/g' server.xml
sed -i 's/port="8080"/port="18080"/g' server.xml
sed -i 's/port="8443"/port="18443"/g' server.xml
sed -i 's/port="8009"/port="18009"/g' server.xml
sed -i 's/redirectPort="8443"/redirectPort="18443"/g' server.xml
```

部署pinpoint-collector.war包
```
cd /home/pp_res/
rm -rf /data/service/pp-col/webapps/*
cp pinpoint-collector-1.5.2.war /data/service/pp-col/webapps/ROOT.war
```
### 安装pinpoint-web
部署war包  
解压Tomcat，将Tomcat重命名移动到指定位置
```
cd /home/pp_res/
tar -zxvf apache-tomcat-8.5.29.tar.gz
mv apache-tomcat-8.5.29/ /data/service/pp-web
```
修改pp-col的Tomcat的配置，主要修改端口，避免与pp-web的Tomcat的端口冲突。我在原本默认的端口前都加了2，下面是替换的shell命令。
```
cd /data/service/pp-web/conf/
sed -i 's/port="8005"/port="28005"/g' server.xml
sed -i 's/port="8080"/port="28080"/g' server.xml
sed -i 's/port="8443"/port="28443"/g' server.xml
sed -i 's/port="8009"/port="28009"/g' server.xml
sed -i 's/redirectPort="8443"/redirectPort="28443"/g' server.xml
```

部署pinpoint-collector.war包
```
cd /home/pp_res/
rm -rf /data/service/pp-web/webapps/*
cp pinpoint-collector-1.7.1.war /data/service/pp-web/webapps/ROOT.war
```
启动Tomcat
```
cd /data/service/pp-web/bin/
./startup.sh
```

查看日志，Tocmat是否启动成功
```
tail -f ../logs/catalina.out
```

## 监控spring boot应用
解压pinpoint-agent-1.7.1.tar.gz并对其进行配置
```
tar zxvf pinpoint-agent-1.7.1.tar.gz -C pinpoint-agent
```
修改pinpoint-agent/pinpoint.config中的配置与collector服务一致。此处因为pinpoint-agent与collector在同一台机器，因此默认配置即可不需要修改。  
启动参数
```
java -javaagent:/pinpoint-agent/pinpoint-bootstrap-1.7.1.jar -Dpinpoint.agentId=spring-boot-app -Dpinpoint.applicationName=spring-boot-app -jar spring-boot-docker-example-1.0.jar
```

## 常见错误
 - agent 和 collector 之间传输数据有使用udp所以要保证udp能够链接到。我一开始使用华为云服务器。没有开udp，所以只能监听到应用，但是并不能获取到数据
 - agent 和 collector 不再一台服务器上时要更改配置文件
 - 自己测试中spring boot 项目不要在idea中直接加参数，这样不能。只能用java -jar 启动
---
## 参考资料
https://github.com/ameizi/DevArticles/issues/166
https://github.com/naver/pinpoint/blob/master/doc/installation.md
http://www.cnblogs.com/yyhh/p/6106472.html