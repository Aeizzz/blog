---
title: 自动化部署搭建过程四
date: 2018-03-28 17:34:36
categories: 自动化部署
tags: spring-boot
top: false
---
## 自动构建docker镜像
因为java项目多使用多模块，所以不同的构建docker images 会很麻烦。
所以我们使用一个maven 的插件docker-maven-plugin
[docker-maven-plugin](https://github.com/spotify/docker-maven-plugin/tree/master)

来说一下使用方法

## pom.xml修改
 - 在 properties 标签内增加
```
<!--配置docker的地址-->
<docker.url>http://210.44.71.52:5555</docker.url>
<registry.url>210.44.71.52:5000</registry.url>
```
 第一个是docker的地址  第二个是docker registey的地址

 - 在build  - plugins 下增加
```xml
<plugin>
    <groupId>com.spotify</groupId>
    <artifactId>docker-maven-plugin</artifactId>
    <version>1.0.0</version>
    <configuration>
        <dockerHost>${docker.url}</dockerHost>
        <imageName>${registry.url}/${project.name}:${project.version}</imageName>
        <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
        <skipDockerBuild>false</skipDockerBuild>
        <resources>
            <resource>
                <targetPath>/</targetPath>
                <directory>${project.build.directory}</directory>
                <include>${project.build.finalName}.jar</include>
            </resource>
        </resources>
        <imageTags>
            <imageTag>${project.version}</imageTag>
        </imageTags>
    </configuration>
    <executions>
        <execution>
            <id>push-image</id>
            <phase>deploy</phase>
            <goals>
                <goal>push</goal>
            </goals>
            <configuration>
                <imageName>${registry.url}/${project.name}:${project.version}</imageName>
            </configuration>
        </execution>
    </executions>
</plugin>
```

 - 在src/main/docker下写Dockerfile文件
 ```dockerfile
FROM java
VOLUME /tmp
ADD demo-0.0.1.jar app.jar
RUN bash -c 'touch /app.jar'
ENV JAVA_OPTS=""
EXPOSE 8080
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
 ```
---
一个最简单的spring-boot的pom.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.example</groupId>
	<artifactId>demo</artifactId>
	<version>0.0.1</version>
	<packaging>jar</packaging>

	<name>demo</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.0.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<!--配置docker的地址-->
		<docker.url>http://210.44.71.52:5555</docker.url>
		<registry.url>210.44.71.52:5000</registry.url>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
            <!--docker插件-->
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>1.0.0</version>
                <configuration>
                    <dockerHost>${docker.url}</dockerHost>
                    <imageName>${registry.url}/${project.name}:${project.version}</imageName>
                    <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
                    <skipDockerBuild>false</skipDockerBuild>
                    <resources>
                        <resource>
                            <targetPath>/</targetPath>
                            <directory>${project.build.directory}</directory>
                            <include>${project.build.finalName}.jar</include>
                        </resource>
                    </resources>
                    <imageTags>
                        <imageTag>${project.version}</imageTag>
                    </imageTags>
                </configuration>
                <executions>
                    <execution>
                        <id>push-image</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>push</goal>
                        </goals>
                        <configuration>
                            <imageName>${registry.url}/${project.name}:${project.version}</imageName>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
		</plugins>
	</build>


</project>

```
    