---
title: CNN 卷积神经网络
date: 2018-03-20 18:55:00
categories: 深度学习
tags: 
 - cnn
 - tensorflow
top: false
---

# 卷积神经网络
卷积神经网络包含输入层、隐藏层和输出层，
隐藏层又包含卷积层和pooling层，图像输入到卷积神经网络后通过卷积来不断的提取特征，每提取一个特征就会增加一个feature map，所以会看到视频教程中的立方体不断的增加厚度，那么为什么厚度增加了但是却越来越瘦了呢，哈哈这就是pooling层的作用喽，pooling层也就是下采样，通常采用的是最大值pooling和平均值pooling，因为参数太多喽，所以通过pooling来稀疏参数，使我们的网络不至于太复杂。

# 卷积神经网络例子
```python
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)


# 结果
def compute_accuracy(v_xs, v_ys):
    global prediction
    y_pre = sess.run(prediction, feed_dict={xs: v_xs, keep_prob: 1})
    correct_prediction = tf.equal(tf.argmax(y_pre, 1), tf.argmax(v_ys, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    result = sess.run(accuracy, feed_dict={xs: v_xs, ys: v_ys, keep_prob: 1})
    return result


def weigth_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


# x输入的值 W 是weight
def conv2d(x, W):
    # strides=[1,x_movement,y_movement,1]
    # Must have strides[1] = strides[4] = 1
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_poll_2x2(x):
    # strides = [1, x_movement, y_movement, 1]
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


xs = tf.placeholder(tf.float32, [None, 784]) / 255 # 28*28
ys = tf.placeholder(tf.float32, [None, 10])

keep_prob = tf.placeholder(tf.float32)
x_images = tf.reshape(xs, [-1, 28, 28, 1])
print(x_images.shape)  # [n_samples,28,28,1]

# convl layer
W_conv1 = weigth_variable([5, 5, 1, 32])  # patch 5*5 ,in size 1, out size 32
b_conv1 = bias_variable([32])
h_conv1 = tf.nn.relu(conv2d(x_images, W_conv1) + b_conv1)  # output size 28 * 28 * 32
h_pool1 = max_poll_2x2(h_conv1)  # output size 14 * 14 * 32

# conv2
W_conv2 = weigth_variable([5, 5, 32, 64])  # patch 5*5 ,in size 32, out size 64
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)  # output size 14 * 14 * 64
h_pool2 = max_poll_2x2(h_conv2)  # output size 7 * 7 * 64

# func1 layer
W_fc1 = weigth_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])
# [n_samples,7,7,64] ->> [n_samples,7*7*64]
h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# func2 layer

W_fc2 = weigth_variable([1024, 10])
b_fc2 = bias_variable([10])
prediction = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

cross_entropy = tf.reduce_mean(-tf.reduce_mean(ys * tf.log(prediction), reduction_indices=[1]))

train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(1000):
        batch_xs, batch_ys = mnist.train.next_batch(100)
        sess.run(train_step, feed_dict={xs: batch_xs, ys: batch_ys, keep_prob: 0.5})
        if i % 50 == 0:
            print(compute_accuracy(mnist.test.images[:1000], mnist.test.labels[:1000]))

```
---

## 代码解读
代码还有很多都不能看懂，一点一点的解读
```
import tensorflow as tf
python from tensorflow.examples.tutorials.mnist import input_data
mnist=input_data.read_data_sets('MNIST_data',one_hot=true)
```
导入头，然后下载数据

---
```
def weight_variable(shape): 
	inital=tf.truncted_normal(shape,stddev=0.1)
	return tf.Variable(initial)
```
定义 `Weight` 变量，输入 `shape` ，返回变量的参数，使用 `tf.truncted_normal` 产生随机变量来进行初始化

---
```
def bias_variable(shape): 
	initial=tf.constant(0.1,shape=shape) 
	return tf.Variable(initial)
```
同样定义bias变量，输入 `shape`，返回变量的一些参数，使用 `tf.constant` 常量函数进行初始化

---
```
def conv2d(x,W):
	return tf.nn.conv2d(x,W,strides=[1,1,1,1]，padding='SAME') 
```
定义卷积，`tf.nn.conv2d`函数是tensorflow里面的二维卷积函数，`x`是图片的所有参数，W是此卷积层的权重，然后定义步长 `strides=[1,1,1,1]` 值，其中 `strides[0]` 和 `strides[0]` 的两个 1 是默认值 ，中间两个1代表padding时在x方向运动一步，y方向运动一步，padding采用的方式是SAME。

---
### 图片处理
```
xs = tf.placeholder(tf.float32, [None, 784]) # 28*28
ys = tf.placeholder(tf.float32, [None, 10])
```
定义输入的placeholder
```
keep_prob = tf.placeholder(tf.float32)
```
我们还定义了dropout的placeholder，它是解决过拟合的有效手段
```
x_image=tf.reshape(xs,[-1,28,28,1])
```
接着呢，我们需要处理我们的xs，把xs的形状变成 `[-1,28,28,1]`，-1代表先不考虑输入的图片例子多少这个维度，后面的1是channel的数量，因为我们输入的图片是黑白的，因此channel是1，例如如果是RGB图像，那么channel就是3。

### 建立卷积层
首先建立第一层卷积  
先定义本层的Weight，本层卷积核的patch的大小为25，因为黑白照片的channel是1所以输入时1，输出是32个featuremap
```
W_conv1 = weigth_variable([5, 5, 1, 32])
```
接着定义bias，它的大小是32，和上面的featuremap相同
```
b_conv1 = bias_variable([32])
```
定义好了Weight和bias，我们就可以定义卷积神经网络的第一个卷积层`h_conv1=conv2d(x_image,W_conv1)+b_conv1`,同时我们对h_conv1进行非线性处理，也就是激活函数来处理喽，这里我们用的是tf.nn.relu（修正线性单元）来处理，要注意的是，因为采用了SAME的padding方式，输出图片的大小没有变化依然是28x28，只是厚度变厚了，因此现在的输出大小就变成了28x28x32
```
h_conv1=tf.nn.relu(conv2d(x_image,W_conv1)+b_conv1)
```
最后我们再进行pooling的处理就ok啦，经过pooling的处理，输出大小就变为了14x14x32
```
h_pool=max_pool_2x2(h_conv1)
```

第一层如下
```
# convl layer
W_conv1 = weigth_variable([5, 5, 1, 32])  # patch 5*5 ,in size 1, out size 32
b_conv1 = bias_variable([32])
h_conv1 = tf.nn.relu(conv2d(x_images, W_conv1) + b_conv1)  # output size 28 * 28 * 32
h_pool1 = max_poll_2x2(h_conv1)  # output size 14 * 14 * 32
```
第二层也是如此
```
# conv2
W_conv2 = weigth_variable([5, 5, 32, 64])  # patch 5*5 ,in size 32, out size 64
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)  # output size 14 * 14 * 64
h_pool2 = max_poll_2x2(h_conv2)  # output size 7 * 7 * 64
```

### 建立全连接
进入全连接层时, 我们通过tf.reshape()将h_pool2的输出值从一个三维的变为一维的数据, -1表示先不考虑输入图片例子维度, 将上一个输出结果展平.
```
#[n_samples,7,7,64]->>[n_samples,7*7*64]
h_pool2_flat=tf.reshape(h_pool2,[-1,7*7*64]) 
```
此时weight_variable的shape输入就是第二个卷积层展平了的输出大小: 7x7x64， 后面的输出size我们继续扩大，定为1024
```
W_fc1=weight_variable([7*7*64,1024]) 
b_fc1=bias_variable([1024])
```
然后将展平后的h_pool2_flat与本层的W_fc1相乘（注意这个时候不是卷积了）
```
h_fc1=tf.nn.relu(tf.matmul(h_pool2_flat,W_fc1)+b_fc1)
```

如果我们考虑过拟合问题，可以加一个dropout的处理
```
h_fc1_drop=tf.nn.dropout(h_fc1,keep_drop)
```
接下来我们就可以进行最后一层的构建了，好激动啊, 输入是1024，最后的输出是10个 (因为mnist数据集就是[0-9]十个类)，prediction就是我们最后的预测值
```
W_fc2=weight_variable([1024,10]) b_fc2=bias_variable([10])
```
我们用softmax分类器（多分类，输出是各个类的概率）,对我们的输出进行分类
```
prediction=tf.nn.softmax(tf.matmul(h_fc1_dropt,W_fc2),b_fc2)
```
