---
title: IP代理池
top: false
date: 2018-02-14 22:40:20
categories: python 
tags: 代理
---
使用flask + redis 实现一个简单的IP代理池

写完好久才来写这个博客，顺便练习了flask，以及操作redis，其中还有一些爬虫

代码仓库[IP_proxy](https://www.7326it.club/hong-ll/IP_proxy)

## 思路
使用爬虫定时抓取各个代理网站上的代理，然后存到redis中  
使用多进程取定时判断代理是否可用，如果不可用则删掉代理

## 要点
使用元类来控制爬虫，这样再添加爬虫的时候就不用增加启动的方法 
使用多进程来定时判断代理是否可用
使用flask来提供api服务




