---
title: 在Ubuntu上安装 Python 3.6.0
date: 2018-01-12 12:55:35
categories: linux
tags: python
---
因为不能使用apt安装，所以要使用源码，自己安装
##  安装
```
wget https://www.python.org/ftp/python/3.6.0/Python-3.6.0.tar.xz
xz -d Python-3.6.0.tar.xz
tar -xvf  Python-3.6.0.tar
cd Python-3.6.0
./configure
make
sudo make install
```

## 测试

```
$ python3.6 --version
Python 3.6.0
```

测试几个新的语法特性：

```
# Formatted string literals
>>> name = 'Ray'
>>> f"Hello {name}."
'Hello Ray.'
```

## Tips

第一次编译安装之后，有可能会发现输入python3.6 之后，方向键失效。
原因是 `readline` 库没有安装。


安装`readline`库
```
sudo apt-get install libreadline-dev
```
重新编译就可以了
