---
title: 康托展开
date: 2018-01-05 13:06:56
categories: Algorithm
tags: 数学
---
先贴一下维基百科上的公式和举例

---
以下来自维基百科[康托展开][1]<br>

## 公式

`X=a[n]*(n-1)!+a[n-1]*(n-2)!+...+a[1]*0!` 其中<br>
$a\_i$ 为整数，并且 $0 \leq a_i \le i , 1 \leq i \leq n。$<br>


$a_i$ 的意义参举例的解释部分<br>
## 举例

例如，3 5 7 4 1 2 9 6 8 展开为 98884。因为<br>

 `X=2*8!+3*7!+4*6!+2*5!+0*4!+0*3!+2*2!+0*1!+0*0!=98884` .

解释：
排列的第一位是3，比3小的数有两个，以这样的数开始的排列有8!个，因此第一项为2\*8!<br>
排列的第二位是5，比5小的数有1、2、3、4，由于3已经出现，因此共有3个比5小的数，这样的排列有7!个，因此第二项为3\*7!<br>
以此类推，直至0\*0!<br>

<!-- more -->

------
再养就很明显了，根据公式求出一个全排列是整个全排列中的第几个
下面来一个题目吧，题目为**2017第八届蓝桥杯模拟赛本科组**中的题目<br>
参赛费好贵，，只能看别人的博客了。。<br>

## 题目

3.排列序数

X星系的某次考古活动发现了史前智能痕迹。<br>
这是一些用来计数的符号，经过分析它的计数规律如下：<br>
（为了表示方便，我们把这些奇怪的符号用a~q代替）<br>

abcdefghijklmnopq 表示0<br>
abcdefghijklmnoqp 表示1<br>
abcdefghijklmnpoq 表示2<br>
abcdefghijklmnpqo 表示3<br>
abcdefghijklmnqop 表示4<br>
abcdefghijklmnqpo 表示5<br>
abcdefghijklmonpq 表示6<br>
abcdefghijklmonqp 表示7<br>
.....

在一处石头上刻的符号是：<br>
bckfqlajhemgiodnp<br>

请你计算出它表示的数字是多少？<br>

请提交该整数，不要填写任何多余的内容，比如说明或注释。<br>

## 代码
```
#include<cstdio>
#include<algorithm>
using namespace std;
long long fact(long long n){
    long long i,s=1;
    for(i=2;i<=n;i++){
        s*=i;
    }
    return s;

}
int main(){
    long long sum=0;
    string s = "bckfqlajhemgiodnp";
    for(int i=0;i<s.size();i++){
        long long Count = 0;
        for(int j=i+1;j<s.size();j++){
            if(s[j]<s[i]) Count++;
        }
        sum=sum+Count*fact(s.size()-1-i);
    }
    printf("%lld\n",sum);
    return 0;
}

```
这个公式不是很难，就是在全排列中找出一个字符比他大的个数，然后乘n-i-1的阶乘<br>



 [1]: https://zh.wikipedia.org/wiki/%E5%BA%B7%E6%89%98%E5%B1%95%E5%BC%80
