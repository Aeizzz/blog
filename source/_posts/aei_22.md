---
title: django学习
date: 2018-01-05 13:27:16
categories: web
tags:
 - django
 - python web
---
# 创建模型
django-admin.py startapp TestModel
编写models 例如  
```
class User(AbstractBaseUser):
    username = models.CharField(max_length=20,unique=True)
    password = models.CharField(max_length=20,blank=True,unique=True)
    phone = models.CharField(max_length=20,blank=True,unique=True)

    class Meta:
        db_table='user'

```
```
$ python manage.py migrate   # 创建表结构
$ python manage.py makemigrations TestModel  # 让 Django 知道我们在我们的模型有一些变更
$ python manage.py migrate TestModel   # 创建表结构
```

## rest-framework
创建serializer类
```
from rest_framework import serializers

class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=20)
    phone = serializers.CharField(max_length=20)
```

编写views
```
# 返回json
class JSONResponse(HttpResponse):
    """docstring for JSONRenderer"""
    '''
    将HttpResponse对象相应的内容转化为json
    '''

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json;charset=utf-8'
        super(JSONResponse, self).__init__(content, **kwargs)

# 注册
class UserRegistAPIview(APIView):
    @csrf_exempt
    def post(self,request):
        '''
        注册json api接口
        '''
        serializer = UserRegisterSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            try:
                User.objects.get(username=data['username'])
                return JSONResponse({'status':'error','message':'用户名已存在'},status=500)
            except User.DoesNotExist:
                user = User.objects.create(username=data['username'],phone=data['phone'])
                user.set_password(data['password'])
                user.save()
                return JSONResponse({'status':'ok','message':'注册用户成功'},status=200)
        else:
            return JSONResponse(serializer.errors,status=500)
```

## django中用户扩展(AbstractUser)
新建一个app
增加models如下
```
class User(AbstractBaseUser):
    username = models.CharField(max_length=20,unique=True)
    studentid = models.CharField(max_length=20,blank=True)
    name = models.CharField(max_length=20,blank=True)
    phone = models.CharField(max_length=20,blank=True)
    major = models.CharField(max_length=20,blank=True)
    grade = models.CharField(max_length=20,blank=True)
    sex = models.CharField(max_length=20,blank=True)
```

在settings中增加
`AUTH_USER_MODEL = 'account.User'`
在models中增加
```
USERNAME_FIELD = "username"
```