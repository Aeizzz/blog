---
title: 巴什博弈
date: 2018-01-05 13:11:07
categories: Algorithm
tags: 博弈论
---
## 概念
什么是巴什博弈：只有一堆n个物品，两个人轮流从这堆物品中取物， 规定每次至少取一个，最多取m个。最后取光者得胜。
## 分析
我们称先进行游戏的叫做先手，后进行游戏的叫做后手<br>
假设 $ n=m+1 $时，无论先手拿走多少后手都能拿走其余的所有，所以后手胜<br>
假设$ n=(m+1)\*r+k $时，（r为任意自然数，$ k<=m $ ）先手拿走k个，后手拿走s个，先手再拿走$ m+1-s $个结果剩下$ (m+1)\*(r-1) $ 个，一次保持这样，那么先手一定会胜<br>
所以我们的出结论，只要给对手留下$ (m+1)\*r $个则自己一定会胜
## 必败必胜状态
只要n不能整除m+1则先手胜，否则后手胜
## 变性
如果我们改变游戏，改成规定最后取光的的输，也就是最后给对手留一个为胜，那该怎么办<br>
若$ (n-1) % (m+1)==0 $则后手胜，否则先手胜
<!-- more -->
## 例题
### 例题一
[HDU-1846-Brave Game][1]<br>
只贴一下题目描述
#### Problem Description
十年前读大学的时候，中国每年都要从国外引进一些电影大片，其中有一部电影就叫《勇敢者的游戏》（英文名称：Zathura），一直到现在，我依然对于电影中的部分电脑特技印象深刻。
今天，大家选择上机考试，就是一种勇敢（brave）的选择；这个短学期，我们讲的是博弈（game）专题；所以，大家现在玩的也是“勇敢者的游戏”，这也是我命名这个题目的原因。
当然，除了“勇敢”，我还希望看到“诚信”，无论考试成绩如何，希望看到的都是一个真实的结果，我也相信大家一定能做到的~
各位勇敢者要玩的第一个游戏是什么呢？很简单，它是这样定义的：
1、  本游戏是一个二人游戏;
2、  有一堆石子一共有n个；
3、  两人轮流进行;
4、  每走一步可以取走1…m个石子；
5、  最先取光石子的一方为胜；
如果游戏的双方使用的都是最优策略，请输出哪个人能赢。

#### 解题思路
最简单的巴什博弈
#### ac代码
```
#include<cstdio>
#include<algorithm>
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n,m;
        scanf("%d%d",&n,&m);
        if(n%(m+1)!=0){
            printf("first\n");

        }
        else printf("second\n");
    }
}
```

### 例题二
题目链接：[HDU-2147-kiki's game][2]
#### 题目描述
就是有一个游戏，在一个$ n\*m $的矩阵中起始位置是$ (1,m) $，走到终止位置$ (n,1) $ ；游戏规则是只能向左，向下，左下方向走，先走到终点的为获胜者。
#### 思路
我们可以寻找必败态和必胜态，我们假设必败态是P，必胜态是N，那么当、时，必胜态必败态矩阵如下：<br>
```
NNNNNNNNN
PNPNPNPNP
NNNNNNNNN
PNPNPNPNP
NNNNNNNNN
PNPNPNPNP
NNNNNNNNN
PNPNPNPNP
```
多试几组样例，我们可以很容易的发现，当和都为奇数的时候，按照题意，应输出“What a pity！”，否则输出“Wonderful！”。
#### ac代码
```
#include<cstdio>
#include<algorithm>
using namespace std;
int main(){
    int n,m;
    while(~scanf("%d%d",&n,&m)){
        if(n==0&&m==0) break;
        if(n%2==1&&m%2==1) printf("What a pity!\n");
        else printf("Wonderful!\n");
    }
}
```

就这个样子

  [1]: http://acm.hdu.edu.cn/showproblem.php?pid=1846
  [2]: http://acm.hdu.edu.cn/showproblem.php?pid=2147