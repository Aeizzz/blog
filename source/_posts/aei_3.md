---
title: 把nginx添加到windows服务中
date: 2018-02-07 16:00:00
categories: nginx
tags: nginx
---
再win下启动和关闭nginx太麻烦，主要是关闭nginx  
所以我想将nginx放在win的服务中，这样启动就容易了

# 下载nginx
再nginx官网下载win版本
[nginx下载](http://nginx.org/en/download.html)

# 下载WinSW
这里借助WinSW这个东西，用来注册服务
在项目主页内找到winsw的下载页面
[winsw](https://github.com/kohsuke/winsw)

# 结束
将下载的文件放在nginx目录下，并修改成`nginx-service.exe`
新建文件`nginx-service.xml`
内容如下
```
<service>
  <id>nginx</id>
  <name>nginx</name>
  <description>nginx</description>
  <env name="path" value="D:\nginx-1.12.2"/>
  <executable>D:\nginx-1.12.2/nginx.exe</executable>
  <arguments>-p D:\nginx-1.12.2</arguments>
  <logpath>D:\nginx-1.12.2/logs/</logpath>      
  <logmode>roll</logmode>
</service>
```
这是我自己设置的作为参照
然后打开命令行输入  `nginx-service install`
全部完成
![nginx](http://olz5306lf.bkt.clouddn.com/nginx.png)
