---
title: UESTC-251-导弹拦截
date: 2018-01-05 13:06:56
categories: Algorithm
tags: 动态规划
---
## 题目链接[UESTC-251-导弹拦截][1]

## 题目描述
某国为了防御敌国的导弹袭击，开发出一种导弹拦截系统。但是这种导弹拦截系统有一个缺陷：虽然它的第一发炮弹能够到达任意的高度，但是以后每一发炮弹都要高于前一发的高度。某天，雷达捕捉到敌国的导弹来袭，并观测到导弹依次飞来的高度，请计算这套系统最多能拦截多少导弹，同时，司令部想知道拦截下来的导弹的高度。拦截来袭导弹时，必须按来袭导弹袭击的时间顺序，不允许先拦截后面的导弹，再拦截前面的导弹。

## Input
第一行是一个整数tt,代表case数。 对于每一个case，第一行是一个整数$n$ ( $1\leq n \leq 100000 $ )； <br>
第二行是n个非负整数，表示第/(n/)枚导弹的高度，按来袭导弹的袭击时间顺序给出，以空格分隔。数据保证高度不会超过$100000$.

## Output
对于每一个case,第一行输出最多能拦截的导弹数，第二行按来袭顺序输出拦截下来的导弹的高度构成的序列，以一个空格隔开。若有不止一种方法可以拦截最多的导弹，输出字典序最小的。

## Sample Input
1
5
1 6 3 5 7
## Sample Output
4
1 3 5 7


## 解题思路
原来做过的导弹拦截都是直接求最长上升子序列，这次的不一样多了一个路径打印，第一次做并不是太会，这个题而且会卡$n^2$,所以必须要用二分去优化<br>
所以整个体考察的知识点就是：最长上升子序列nlogn算法(二分)+路径打印<br>
关于最长上升子序列nlogn算法请看这[最长上升子序列nlogn算法][2]

我们对于路径的记录：<br>
从后往前,主要代码<br>
```
 int p=len,T=inf;
for(int i=n;i>0;i--){
    if(!p) break;
    if(dp[i]==p && a[i]<T){
        ans[p--]=a[i];
        T=a[i];
    }
}
for(int i=1;i<=len;i++){
    if(i==1) printf("%d",ans[i]);
    else printf(" %d",ans[i]);
}
printf("\n");
```

## AC代码
```
#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn = 100000+10;
const int inf = 1<<30;
int a[maxn];
int dp[maxn];
int ans[maxn];
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n;
        scanf("%d",&n);
        for(int i=1;i<=n;i++){
            scanf("%d",&a[i]);
        }
        ans[1]=a[1];
        int len=1;
        dp[1]=1;
        for(int i=2;i<=n;i++){
            if(a[i]>ans[len]){
                ans[++len]=a[i];
                dp[i]=len;
            }else{
                int c=lower_bound(ans+1,ans+len+1,a[i])-ans;
                dp[i]=c;
                if(a[i]<ans[c]) ans[c]=a[i];
            }
        }
        printf("%d\n",len);
        int p=len,T=inf;
        for(int i=n;i>0;i--){
            if(!p) break;
            if(dp[i]==p && a[i]<T){
                ans[p--]=a[i];
                T=a[i];
            }
        }
        for(int i=1;i<=len;i++){
            if(i==1) printf("%d",ans[i]);
            else printf(" %d",ans[i]);
        }
        printf("\n");
    }
    return 0;
}

```


  [1]: http://acm.uestc.edu.cn/#/problem/show/251
  [2]: http://blog.csdn.net/u013806814/article/details/38711125