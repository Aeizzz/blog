---
title: 算法分类
date: 2018-01-05 13:15:53
categories: Algorithm
tags: 算法分类
---
来源 **[知乎](https://www.zhihu.com/question/51614608/answer/127751306)** 
Hash Table Problem - 5183
ST Table Problem - 3183
树状数组单点修改区间查询 Problem - 1754 
树状数组区间修改单点查询 Problem - 1608 
树状数组区间修改区间查询 Problem - 3468 
线段树单点修改区间查询 Problem - 1754 
线段树区间修改单点查询 Problem - 1608 
线段树区间修改区间查询 Problem - 3468 
线段树合并 Problem - 3667 
线段树扫描线 Problem - 1542 
线段树区间GCD维护 Problem - 5726 
李超线段树 Problem - 1568 
主席树 Problem - 4729 
Splay Problem - 3487
Treap Problem - 4585
替罪羊树 Problem - 3065 
树链剖分 Problem - 3804 
Link/Cut Tree Problem - 5398
K-D Tree Problem - 4400
块状链表 Problem - 4366 
莫队算法 Problem - 5381 
树上莫队算法 Problem - COT2 
左偏树 Problem - 3031 
堆 Problem - 3673 
并查集 Problem - 1272 
带权并查集 Problem - 3047 
链表 Problem - 1413 
双向链表 Problem - 4699 
双端队列 Problem - 5929 
栈 Problem - 1022 
单调栈 Problem - 1506 
最小生成树 Problem - 2682 
次小生成树 Problem - 4756 
最优比率生成树 Problem - 2728 
曼哈顿最小生成树 Problem - 3241 
斯坦纳树 Problem - 3311 
完美消除序列 Problem - 1006 
Huffman编码 Problem - 2527
最小树形图 Problem - 2121<br
树的重心 Problem - 4118 
最近公共祖先 Problem - 2586 
生成树计数 Problem - 4305 
二叉树遍历 Problem - 1710 
割点 Problem - 4587 
割边 Problem - 4738 
边双连通分量 Problem - 4612 
点双连通分量 Problem - 3394 
强连通分量 Problem - 4635 
Dominator Tree Problem - 4694
最大流 Problem - 3549 
最小割 Problem - 3917 
最小费用最大流 Problem - 1533 
线性规划 Problem - 5699 
二分图判定 Problem - 4751 
二分图最大匹配 Problem - 5093 
最大独立集 Problem - 1068 
最大点权独立集 Problem - 1565 
2-SAT Problem - 4421
单源最短路 Problem - 3795 
全源最短路 Problem - 3631 
次短路 Problem - 1688 
第K短路 Problem - 2449 
差分约束 Problem - 4598 
拓扑排序 Problem - 2647 
欧拉回路 Problem - 2894 
最大团 Problem - 1530 
稳定婚姻匹配 Problem - 1914 
Dancing Links Problem - 3111
插头动态规划 Problem - 1693 
区间动态规划 Problem - 5900 
环形动态规划 Problem - 3506 
树形动态规划 Problem - 2196 
概率动态规划 Problem - 4336 
状态压缩动态规划 Problem - 3920 
整数划分动态规划 Problem - 5230 
单调队列优化动态规划 Problem - 3401 
斜率优化动态规划 Problem - 2829 
四边形不等式优化动态规划 Problem - 3516 
01背包 Problem - 1171
完全背包 Problem - 4508 
多重背包 Problem - 2844 
混合背包 Problem - 5410 
二维费用背包 Problem - 3496 
分组背包 Problem - 1712 
依赖背包 Problem - 1561 
KMP Problem - 4763
Extended KMP Problem - 4333
Manacher Problem - 3613
Trie Problem - 1671
Aho-Corasick Automaton Problem - 2296
后缀数组 Problem - 1403 
后缀自动机 Problem - 4416 
后缀树 Problem - 3518 
巴什博弈 Problem - 2147 
Nim游戏 Problem - 2509
威佐夫博弈 Problem - 1527 
树上删边游戏 Problem - 5299 
Sprague-Grundy Function Problem - 4155
Pick定理 Problem - 3775
半平面交 Problem - 3761 
旋转卡壳 Problem - 3934 
凸包 Problem - 1348 
三维点积 Problem - 3692 
三维叉积 Problem - 1174 
三维凸包 Problem - 4266 
仿射变换 Problem - 4087 
欧拉筛法 Problem - 2161 
欧拉函数 Problem - 4483 
莫比乌斯反演 Problem - 4746 
Miller-Rabin素数测试 Problem - 2138
Prime-counting Function Problem - 5901
Pollard's rho整数分解 Problem - 3864
最大公约数 Problem - 1019 
最小公倍数 Problem - 1108 
扩展欧几里得 Problem - 2669 
乘法逆元 Problem - 1576 
线性逆元 Problem - 5673 
高斯消元 Problem - 3364 
模线性方程组 Problem - 1573 
异或线性基 Problem - 3949 
行列式 Problem - 4305 
容斥原理 Problem - 4135 
置换群 Problem - 5495 
Lucas定理 Problem - 3944
组合数取模 Problem - 3944 
BSGS Problem - 2417
Extended BSGS Problem - 2815
快速沃尔什变换 Problem - 5909 
快速傅里叶变换 Problem - 5307 
自适应辛普森积分法 Problem - 1724 
Bell数 Problem - 2512
Catalan数 Problem - 1134
逆序对 Problem - 4911 
九余数定理 Problem - 1163 
抽屉原理 Problem - 5776 
康托展开 Problem - 1430 
蔡勒公式 Problem - 2005 
错排公式 Problem - 1465 
进制转换 Problem - 2031 
高精度 Problem - 1042 
快速幂 Problem - 1061 
矩阵快速幂 Problem - 2604 
母函数 Problem - 2110 
深度优先搜索 Problem - 1518 
记忆化搜索 Problem - 5001 
广度优先搜索 Problem - 1026 
双向广度优先搜索 Problem - 3085 
A* Problem - 1043
IDA* Problem - 1667
对抗搜索 Problem - 4597 
最长不下降子序列 Problem - 1950 
最长公共子序列 Problem - 1159 
最长公共前缀 Problem - 4691 
最大子矩阵 Problem - 4328 
平面最近点对 Problem - 1007 
平面最近圆对 Problem - 3124 
贪心法 Problem - 1789 
双指针 Problem - 5672 
二分法 Problem - 2199 
三分法 Problem - 2298 
二分答案 Problem - 4190 
二分查找 Problem - 2141 
CDQ分治 Problem - 4742
模拟退火 Problem - 2297 