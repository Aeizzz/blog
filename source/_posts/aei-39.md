---
title: 云主机开启swap
top: false
date: 2018-07-07 09:02:55
categories: linux
tags: 
 - swap
 - linux
 - 云服务器
---

一般的云服务器都不开启swap交换空间，原因当然是通过取消swap功能可以降低磁盘IO的占用率来让用户购买更多的内存、提高磁盘寿命和性能。

如何启用swap分区

## 查看当前系统中是否已经启用swap分区
```
cat /proc/swaps   
top
```

## 如果没有启用swap分区功能，则新建一个专门的文件用于swap分区
```
dd if=/dev/zero of=/data/swap bs=512 count=8388616
```

注：此文件的大小是count的大小乘以bs大小，上面命令的大小是4294971392，即4GB

## 通过mkswap命令将上面新建出的文件做成swap分区

```
mkswap /data/swap
```

## 查看内核参数vm.swappiness中的数值是否为0，如果为0则根据实际需要调整成30或者60
```
cat /proc/sys/vm/swappiness   
sysctl -a | grep swappiness    
sysctl -w vm.swappiness=60
```

若想永久修改，则编辑/etc/sysctl.conf文件

## 启用此交换分区的交换功能
```
swapon /data/swap   
echo "/data/swap swap swap defaults    0  0" >> /etc/fstab
```

## 如何关闭swap分区？
```
swapoff /data/swap   
swapoff -a >/dev/null
```

