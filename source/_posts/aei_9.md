---
title: 使用autossh进行内网穿透
date: 2018-01-05 15:59:11
categories: linux
tags: ssh
---
使用ssh进行内网穿透，不写原理，，因为我也不懂
使用方法
为了维持稳定，使用autossh来代替ssh
外网的服务器，用户名，地址 a.site

以下在内网机上使用

## 安装autossh
```
sudo apt install autossh
```
## 建立链接
```
autossh -p 22 -M 6777 -NR 0.0.0.0:6766:localhost:22 ubuntu@a.site
```
这样就把本地的22端口映射到6766端口
想要后台运行的话增加-f参数
```
autossh -p 22 -M 6777 -fNR 0.0.0.0:6766:localhost:22 ubuntu@a.site
```

## 免密登陆

在本地计算机上建立密钥
```
ssh-keygen -t rsa
```
直接一路回车，
然后
`ssh-copy-id ubuntu@a.site`
然后把本地公钥复制到公网服务器，这样就可以在公网服务器免密登陆



