---
title: sql语言学习
date: 2018-01-08 22:53:00
categories: SQL
tags: SQL
---
要考数据库了，来复习数据库
从牛客上找了点sql的题目来刷一刷


从前往后板刷
# 查找最晚入职员工的所有信息
## 题目描述
查找最晚入职员工的所有信息
```
CREATE TABLE `employees` (
`emp_no` int(11) NOT NULL,
`birth_date` date NOT NULL,
`first_name` varchar(14) NOT NULL,
`last_name` varchar(16) NOT NULL,
`gender` char(1) NOT NULL,
`hire_date` date NOT NULL,
PRIMARY KEY (`emp_no`));
```
## 答案
```
select * 
from employees
where hire_date = (select max(hire_date) from employees)
```
使用子查询查找出最大的入职时间

# 查找入职员工时间排名倒数第三的员工所有信息
## 题目描述
查找入职员工时间排名倒数第三的员工所有信息
```
CREATE TABLE `employees` (
`emp_no` int(11) NOT NULL,
`birth_date` date NOT NULL,
`first_name` varchar(14) NOT NULL,
`last_name` varchar(16) NOT NULL,
`gender` char(1) NOT NULL,
`hire_date` date NOT NULL,
PRIMARY KEY (`emp_no`));
```
## 答案
```
select * from employees order by hire_date desc limit 2,1
```
首选查询入职时间，然后从大到小排序，使用limit分页,每页一个，第三页，为2(因为从0开始)

# 查找当前薪水详情以及部门编号dept_no
## 题目描述
查找各个部门当前`(to_date='9999-01-01')`领导当前薪水详情以及其对应部门编号dept_no
```
CREATE TABLE `dept_manager` (
`dept_no` char(4) NOT NULL,
`emp_no` int(11) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`dept_no`));

CREATE TABLE `salaries` (
`emp_no` int(11) NOT NULL,
`salary` int(11) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`from_date`));
```
## 答案
```
select s.emp_no,s.salary,s.from_date,s.to_date,dm.dept_no 
from salaries s left join dept_manager dm on dm.emp_no = s.emp_no  
where dm.to_date = '9999-01-01' and s.to_date = '9999-01-01'
```
使用salaries(薪水)为主表，然后左连接，使两个表都为`to_date = '9999-01-01'`

# 查找所有已经分配部门的员工的last_name和first_name
## 题目描述
查找所有已经分配部门的员工的last_name和first_name
```
CREATE TABLE `dept_emp` (
`emp_no` int(11) NOT NULL,
`dept_no` char(4) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`dept_no`));

CREATE TABLE `employees` (
`emp_no` int(11) NOT NULL,
`birth_date` date NOT NULL,
`first_name` varchar(14) NOT NULL,
`last_name` varchar(16) NOT NULL,
`gender` char(1) NOT NULL,
`hire_date` date NOT NULL,
PRIMARY KEY (`emp_no`));
```
## 答案
```
select e.last_name,e.first_name,de.dept_no
from dept_emp de left join employees e on e.emp_no = de.emp_no
```
同样使用左连接
dept_emp(部门表)做为主表，因为有的员工没有被分配部门，所以如果用员工作为主表则会出现多余的没有分配部门的员工。

# 查找所有员工的last_name和first_name以及对应部门编
## 题目描述
查找所有员工的last_name和first_name以及对应部门编号dept_no，也包括展示没有分配具体部门的员工
```
CREATE TABLE `dept_emp` (
`emp_no` int(11) NOT NULL,
`dept_no` char(4) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`dept_no`));

CREATE TABLE `employees` (
`emp_no` int(11) NOT NULL,
`birth_date` date NOT NULL,
`first_name` varchar(14) NOT NULL,
`last_name` varchar(16) NOT NULL,
`gender` char(1) NOT NULL,
`hire_date` date NOT NULL,
PRIMARY KEY (`emp_no`));
```
## 答案
```
select e.last_name,e.first_name,de.dept_no
from employees e left join dept_emp de on e.emp_no = de.emp_no

```
和上一个题基本一样，但是多一句话
包括展示没有分配具体部门的员工
因为要显示没有分配具体部门的员工。
所以使用员工表进行左连接，就可以了

# 查找所有员工入职时候的薪水情况
## 题目描述
查找所有员工入职时候的薪水情况，给出emp_no以及salary， 并按照emp_no进行逆序
```
CREATE TABLE `employees` (
`emp_no` int(11) NOT NULL,
`birth_date` date NOT NULL,
`first_name` varchar(14) NOT NULL,
`last_name` varchar(16) NOT NULL,
`gender` char(1) NOT NULL,
`hire_date` date NOT NULL,
PRIMARY KEY (`emp_no`));

CREATE TABLE `salaries` (
`emp_no` int(11) NOT NULL,
`salary` int(11) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`from_date`));
```
## 答案
```
select s.emp_no,s.salary
from salaries s left join employees e on e.emp_no = s.emp_no
where s.from_date = e.hire_date 
order by s.emp_no desc
```
这个也不难，
首先薪水表作为主表，然后左连接，并且使入职时间等于薪水发放的时间，然后根据emp_no逆序，使用order by (*) desc

# 查找薪水涨幅超过15次的员工号emp_no以及其对应的涨幅次数t
## 题目描述
查找薪水涨幅超过15次的员工号emp_no以及其对应的涨幅次数t
```
CREATE TABLE `salaries` (
`emp_no` int(11) NOT NULL,
`salary` int(11) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`from_date`));
```
## 答案
这个就比较简单了，直接使用分组查询并且使查询出来的数大于15就可以了(使用子语句，分组查询的子语句使用having)
```
select emp_no,count(emp_no)
from salaries 
group by emp_no 
having count(emp_no) >15
```

# 找出所有员工当前薪水salary情况
## 题目描述
找出所有员工当前(to_date='9999-01-01')具体的薪水salary情况，对于相同的薪水只显示一次,并按照逆序显示
```
CREATE TABLE `salaries` (
`emp_no` int(11) NOT NULL,
`salary` int(11) NOT NULL,
`from_date` date NOT NULL,
`to_date` date NOT NULL,
PRIMARY KEY (`emp_no`,`from_date`));
```
## 答案
比较简单
直接查询根据薪水逆序排序，然后使用`distinct`关键字去掉重复的元素
```
select distinct salary
from salaries
where to_date='9999-01-01' 
order by salary desc
```

# 创建一个actor表，包含如下列信息
## 题目描述
创建一个actor表，包含如下列信息
列表	类型	是否为NULL	含义
actor_id	smallint(5)	not null	主键id
first_name	varchar(45)	not null	名字
last_name	varchar(45)	not null	姓氏
last_update	timestamp	not null	最后更新时间，默认是系统的当前时间

## 答案
直接创建
```
create table actor (
    actor_id smallint(5) not null primary key,
    first_name varchar(45) not null,
    last_name varchar(45) not null,
    last_update timestamp not null default(datetime('now','localtime'))
)
```