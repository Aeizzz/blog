---
title: 自动化部署搭建过程三
date: 2018-03-28 17:02:22
categories: 自动化部署
tags: docker registey
top: false
---

## 安装docker registey
我选择在 jenkins 相同的服务器上安装docker 仓库
同样使用docker安装

```
docker run -d --name registry -p 5000:5000 --restart=always -v /opt/registry/:/var/lib/registry/ registry:2.6.0
```
配置 daemon.json， 去掉docker默认的https的访问
```
vim /etc/docker/daemon.json
里面的内容是一个json对象,加上一项insecure-registries，地址自己更改为 (注：所有需要使用这个仓库的服务器都要进行更改)：
{
"insecure-registries":["210.44.71.52:5000"]
}
```


测试 搭建是否正常
```
curl http://210.44.71.52:5000/v2/_catalog
```

## 安装 docker registey ui
同使用docker安装 
```
sudo docker run -it -p 8090:8080 --name registry-web --link registry \
           -e REGISTRY_URL=http://210.44.71.52:5000/v2 \
           -e REGISTRY_BASIC_AUTH="b3BzOjEyMzEyMzEyMwo=" \
           -e REGISTRY_NAME=210.44.71.52:5000 -d hyper/docker-registry-web
```
其中  link是链接的registry的docker名
地址一定要配置正确
启动以后访问地址即可

---
以上是有关docker registey的相关配置
