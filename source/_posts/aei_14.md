---
title: 自动化部署搭建过程二
date: 2018-03-28 16:50:03
categories: 自动化部署
tags: jenkins
top: false
---
## 搭建jenkins
上一次是搭建gitlab，这一次也是用相同的环境，更换了一台服务器，
### 使用docker安装jenkins
```bash
docker run -d -p 8080:8080 -p 50000:50000 --name jenkins -u root \
        -v ~/jenkins:/var/jenkins_home \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v /usr/bin/docker:/usr/bin/docker \
        -v /usr/share/maven:/usr/local/maven \
        -v /usr/lib/jvm/java-8-openjdk-amd64:/usr/local/jdk \
        jenkinsci/jenkins:latest
```
启动以后就可以直接访问链接，查看jenkins，
第一次登陆需要秘钥
```
docker logs jenkins
```
查看秘钥

登陆的时候安装需要的插件，如果不知道装什么的话就直接选择推荐安装

## 安装插件
在 系统管理 > 插件管理 中
搜索Maven，选择下面这个插件安装
![](http://olz5306lf.bkt.clouddn.com/20180328164934982/20180328045539887.png)
搜索 git   gitlab 安装一下两个插件
![](http://olz5306lf.bkt.clouddn.com/20180328164934982/20180328045736528.png)

## 在docker中安装maven
其实应该可以使用宿主机上的maven，然后映射到容器中，我第一次没有成功，所以自己在docker中安装maven
```
docker exec -it jenkins /bin/bash
进入docker容器中
apt update && apt install maven && vim
安装maven 和 vim
```
## 配置jenkins
配置  全局系统  配置中
选择 git  
![](http://olz5306lf.bkt.clouddn.com/20180328164934982/20180328050146672.png)
配置maven
![](http://olz5306lf.bkt.clouddn.com/20180328164934982/20180328050120671.png)


---
以上 jenkins基本配置完成
