---
title: python-常见面试整理
date: 2018-02-10 21:40:00
categories: python
tags: 面试整理
top: true
---

## 如何反序一个迭代
```
# 如果是一个list
tempList = [1,2,3,4]
tempList.reverse()
for x in tempList:
    print(x)

#如果不是list
tempTuple = (1,2,3,4)
for i in range(len(tempTuple)-1,-1,-1):
    print(tempTuple[i])
## 当然还有这个方法
print(tempTuple[::-1])

4 3 2 1 
------
4 3 2 1 
(4, 3, 2, 1)

```
## 如何用Python来进行查询和替换一个文本字符串？
```
# 使用Python中的replace()可以替换一个文本字符串
tempstr = 'Hello Java,Hello Python,Use javaScript!'
print(tempstr.replace('Hello','Bye'))

# 使用Python中的sub()，可以用来查找并替换字符串，sub是通过正则来匹配的
import re
rex = r'(Hello|Use)'
print(re.sub(rex,'Bye',tempstr))

Bye Java,Bye Python,Use javaScript!
Bye Java,Bye Python,Bye javaScript!
```

## 使用Python实现单例模式
所谓的单利就是从一个类从始至终只产生一个实例
 ### Python模板
 这个是最简单的方法
 ```
# mysingleton.py
class My_Singleton(object):
    def foo(self):
        pass

my_singleton = My_Singleton()

# to Use
from .mysingleton import my_singleton
my_singleton.foo()
 ```
 这样，实例化以后再别处引用

 ### 使用__new__方法
在__new__方法中把类实例绑定到类变量_instance上，如果cls._instance为None表示该类还没有实例化过，实例化该类并返回。如果cls_instance不为None表示该类已实例化，直接返回cls_instance
```


class SingleTon(object):

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls,'_instance'):
            cls._instance = object.__new__(cls,*args, **kwargs)
        return cls._instance

class TestClass(SingleTon):
    a = 1

test = TestClass()
test2 = TestClass()
print(test.a,test2.a)

test.a = 2
print(test.a,test2.a)

print(id(test),id(test2))

--------------------------------------
1 1
2 2
1761048442584 1761048442584
```

### 使用装饰器(decorator)
```


def SingleTon(cls,*args, **kwargs):
    instances = {}
    def _sigleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return _sigleton

@SingleTon
class TestClass(object):
    a = 1

test1 = TestClass()
test2 = TestClass()
print(test1.a,test2.a)

test1.a = 2
print(test1.a,test2.a)

print(id(test1),id(test2))

-------------------
1 1
2 2
1617219944288 1617219944288
```

### 共享属性
所谓单例就是所有的引用（实例，对象）拥有相同的属性和方法，同一个类的实例天生都会有相同的方法，那我们只需要保证同一个类所产生的实例都具有相同的属性。所有实例共享属性最简单直接的方法就是共享__dict__属性指向。
```
1 1
2 2
2510094120664 2510094119600
```
共享属性的属性都是一样的，而id是不同的

## Python的函数参数传递
```
a = 1


def fun(a):
    a = 2


print(a)

b = []


def fun1(b):
    b.append(1)


fun1(b)
print(b)

----------
1
[1]
```
关于以上代码：Python中string、tuple、number属于不可更改对象，而list和dict属于可修改对象。

## 类变量和实例变量
类变量就是供给类使用的变量，实例变量就是供给实例使用的变量。看以下代码
 - 上半部分：name是字符串（不可更改对象），实例变量p1.name一开始指向了类变量name="aaa"，但是在实例的作用域把类变量的引用改变了，就变成了一个实例变量self.name不再引用Person的类变量name 
 - 下班部分：name是list（可更改对象）
```
class Person:
    name = 'aaa'

p1 = Person()
p2 = Person()
p1.name = 'bbb'
print(p1.name)  # bbb
print(p2.name)  # aaa
print(Person.name)  # aaa

---------------------

class Person:
    name = []

p1 = Person()
p2 = Person()
p1.name.append(1)
print(p1.name)  # [1]
print(p2.name)  # [1]
print(Person.name)  # [1]
```

## 以下代码将输出什么？（考察list）
```
tempList = [1,2,3,4]
print(tempList[10:])
--------
[]
```
试图访问一个列表的以超出列表成员数作为开始索引的切片将不会导致IndexError，并且仅仅返回一个空list

## Python中单下划线和双下划线
```
>>> class MyClass():
	def __init__(self):
		self.__aa = 'Hello'
		self._bb = ',World!'

>>> mc = MyClass()
>>> print(mc.__aa)
Traceback (most recent call last):
  File "<pyshell#6>", line 1, in <module>
    print(mc.__aa)
AttributeError: 'MyClass' object has no attribute '__aa'
>>> print(mc._bb)
,World!
>>> print(mc.__dict__)
{'_MyClass__aa': 'Hello', '_bb': ',World!'}
>>> 
```
__foo__:一种约定,Python内部的名字,用来区别其他用户自定义的命名,以防冲突.

_foo:一种约定,用来指定变量私有.程序员用来指定私有变量的一种方式.

__foo:这个有真正的意义:解析器用_classname__foo来代替这个名字,以区别和其他类相同的命名.
详见[Python的类的下划线命名有什么不同？](https://www.zhihu.com/question/19754941)

