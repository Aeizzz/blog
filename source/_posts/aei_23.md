---
title: flask使用celery
top: false
date: 2018-02-23 11:37:12
categories: python
tags: celery
---
再生产模式中使用celery

看一下flask项目结构目录
```
.
├── README.md
├── app
│   ├── __init__.py
│   ├── config.py
│   ├── forms
│   ├── models
│   ├── tasks
│   │   ├── __init__.py
│   │   └── email.py
│   └── views
│   │   ├── __init__.py
│   │   └── account.py
├── celery_worker.py
├── manage.py
└── wsgi.py
```
 - 项目的根目录下，有个 celery_worker.py 的文件，这个文件的作用类似于 wsgi.py，是启动 Celery worker 的入口。
 - app 包里是主要业务代码，其中 tasks 里定义里一系列的 task，提供给其他模块调用。

---
## 主要代码
 - app/config.py
 ```
 class BaseConfig(object):
    CELERY_BROKER_URL = 'redis://localhost:6379/2'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/2'
    CELERY_TASK_SERIALIZER = 'json'
 ```
 - app/__init__.py
 ```
 from celery import Celery
from flask import Flask

from app.config import BaseConfig

celery = Celery(__name__, broker=BaseConfig.CELERY_BROKER_URL)


def create_app():
    app = Flask(__name__)
    # ....
    celery.conf.update(app.config)	# 更新 celery 的配置
    # ...
    return app
 ```
 - app/tasks/email.py
 ```
 from flask import current_app
from celery.utils.log import get_task_logger

from app import celery

logger = get_task_logger(__name__)


@celery.task
def send_email(to, subject, content):
    app = current_app._get_current_object()
    subject = app.config['EMAIL_SUBJECT_PREFIX'] + subject
    logger.info('send message "%s" to %s', content, to)
    return do_send_email(to, subject, content)
 ```
 - app/views/account.py
 ```
 import uuid

from flask import Blueprint, request,jsonify

from app.tasks.email import send_email

bp_account = Blueprint('account', __name__)


@bp_account.route('/password/forgot/', methods=['POST'])
def reset_password():
    email = request.form['email']
    token = str(uuid.uuid4())
    content = u'请点击链接重置密码：http://example.com/password/reset/?token=%s' % token
    send_email.delay(email, content)
    return jsonify(code=0, message=u'发送成功')
 ```

 - celery_worker.py
```
from app import create_app, celery

app = create_app()
app.app_context().push()
```
这个 celery_worker.py 文件有两个操作：
 1. 创建一个 Flask 实例
 2. 推入 Flask application context
第一个操作很简单，其实也是初始化了 celery 实例。

## 运行
在项目的根路径下启动 Celery worker:
```
celery worker -A celery_worker.celery -l INFO
```

