---
title: 从零开始做一个汽车分类器
date: 2018-03-25 14:12:24
categories: 深度学习
tags: tensorflow
top: false
---
汽车的数据在网上找到的，地址为 http://archive.ics.uci.edu/ml/datasets/Car+Evaluation
简单介绍数据
汽车状态分四类：
 - unacc (Unacceptable 状况很差)
 - acc (Acceptable 状况一般)
 - good (Good 状况好)
 - vgood (Very good 状况非常好)
 
判断汽车的好坏：
 - buying (购买价: vhigh, high, med, low)
 - maint (维护价: vhigh, high, med, low)
 - doors (几个门: 2, 3, 4, 5more)
 - persons (载人量: 2, 4, more)
 - lug_boot (贮存空间: small, med, big)
 - safety (安全性: low, med, high)
 
数据格式为
```
buying	maint	doors	persons	 lug_boot    safety    condition 
vhigh,  vhigh,  2,       2,      small,      low,      unacc
vhigh,  vhigh,  2,       2,      small,      med,      unacc
vhigh,  vhigh,  2,       2,      small,      high,     unacc
vhigh,  vhigh,  2,       2,      med,        low,      unacc
vhigh,  vhigh,  2,       2,      med,        med,      unacc
vhigh,  vhigh,  2,       2,      med,        high,     unacc
vhigh,  vhigh,  2,       2,      big,        low,      unacc
vhigh,  vhigh,  2,       2,      big,        med,      unacc
vhigh,  vhigh,  2,       2,      big,        high,     unacc
```
## 数据处理
我们还需要进行数据处理。
把所有不是数字的都要转化为数字

```python
# 数据处理
from urllib.request import urlretrieve

import pandas as pd


def load_data(download=True):
    '''
    下载数据
    :param download:
    :return:
    '''
    if download:
        data_path,_=urlretrieve('http://archive.ics.uci.edu/ml/machine-learning-databases/car/car.data','car.csv')
        print('Downloaded to car.csv')

        col_names = ['buying','maint','doors','persons','lug_boot','safety','class']
        data = pd.read_csv('car.csv',names=col_names)
        return data

def convert2onehot(data):
    '''
    处理数据
    :param data:
    :return:
    '''
    return pd.get_dummies(data,prefix=data.columns)


if __name__ == '__main__':
    data = load_data(download=True)
    new_data = convert2onehot(data)

    print(data.head())
    

    for name in data.keys():
        print(name,pd.unique(data[name]))

    print(new_data.head())

    for name in new_data.keys():
        print(name,pd.unique(new_data[name]))
```

## 搭建模型
导入数据 
刚才写的处理数据，想在可以导入
```
import numpy as np
import tensorflow as tf
import data_processing
# 获取数据，数据处理
data = data_processing.load_data(download=False)
new_data = data_processing.convert2onehot(data)
```

加工好数据以后, 为了比较严谨地测试模型的准确率, 我们首先打乱数据的顺序, 然后将训练和测试数据以 7/3 比例分开.
```
# 分成训练集和测试机
sep = int(0.7*len(new_data))
train_data = new_data[:sep]
test_data = new_data[sep:]
```

接着我们就搭建神经网络, input 数据的后面4个是真实数据的4类型的 onehot 形式. 我们添加两层隐藏层, 用 softmax 来输出每种类型的概率. 使用 tensorflow 的功能计算 loss 和 accuracy.
```
tf_input = tf.placeholder(tf.float32,[None,25],'input') # 25列
# 分开x和y
tfx = tf_input[:,:21]
tfy = tf_input[:,21:]

# 两个隐藏层 一个输出层
l1 = tf.layers.dense(tfx,128,tf.nn.relu,name='l1')
l2 = tf.layers.dense(l1,128,tf.nn.relu,name='l2')
out = tf.layers.dense(l2,4,tf.nn.relu,name='l3')

prediction = tf.nn.softmax(out,name='pred')

loss = tf.losses.softmax_cross_entropy(onehot_labels=tfy,logits=out)

accuracy = tf.metrics.accuracy(     # return (acc, update_op), and create 2 local variables
    labels=tf.argmax(tfy,axis=1),predictions=tf.argmax(out,axis=1),
)[1]

```

最后训练网络
```
opt = tf.train.GradientDescentOptimizer(0.1)
train_op = opt.minimize(loss)

sess = tf.Session()
sess.run(tf.group(tf.global_variables_initializer(),tf.local_variables_initializer()))
for t in range(4000):
    batch_index = np.random.randint(len(train_data),size=32)
    sess.run(train_op,{tf_input:train_data[batch_index]})
    if t%50 == 0:
        acc_,pred_ ,loss_ = sess.run([accuracy,prediction,loss],{tf_input:test_data})
        print("Step: %i" % t,"| Accurate: %.2f"%acc_,"| Loss: %.2f"%loss_,)
```



## 完整代码
```


import numpy as np
import tensorflow as tf

# from data_processing import load_data, convert2onehot
import data_processing
# 获取数据，数据处理
data = data_processing.load_data(download=False)
new_data = data_processing.convert2onehot(data)

new_data = new_data.values.astype(np.float32)
np.random.shuffle(new_data)

# 分成训练集和测试机
sep = int(0.7*len(new_data))
train_data = new_data[:sep]
test_data = new_data[sep:]


tf_input = tf.placeholder(tf.float32,[None,25],'input') # 25列
# 分开x和y
tfx = tf_input[:,:21]
tfy = tf_input[:,21:]

# 两个隐藏层 一个输出层
l1 = tf.layers.dense(tfx,128,tf.nn.relu,name='l1')
l2 = tf.layers.dense(l1,128,tf.nn.relu,name='l2')
out = tf.layers.dense(l2,4,tf.nn.relu,name='l3')

prediction = tf.nn.softmax(out,name='pred')

loss = tf.losses.softmax_cross_entropy(onehot_labels=tfy,logits=out)

accuracy = tf.metrics.accuracy(    # return (acc, update_op), and create 2 local variables
    labels=tf.argmax(tfy,axis=1),predictions=tf.argmax(out,axis=1),
)[1]

opt = tf.train.GradientDescentOptimizer(0.1)
train_op = opt.minimize(loss)

sess = tf.Session()
sess.run(tf.group(tf.global_variables_initializer(),tf.local_variables_initializer()))
for t in range(4000):
    batch_index = np.random.randint(len(train_data),size=32)
    sess.run(train_op,{tf_input:train_data[batch_index]})
    if t%50 == 0:
        acc_,pred_ ,loss_ = sess.run([accuracy,prediction,loss],{tf_input:test_data})
        print("Step: %i" % t,"| Accurate: %.2f"%acc_,"| Loss: %.2f"%loss_,)

```
